/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 ******************************************************************************/
package net.islandearth.mcrealistic.language;

import org.bukkit.entity.Player;

import lombok.Getter;
import lombok.Setter;
import net.islandearth.languagy.language.Translator;

public enum Lang {
    NO_PERMISSION("no_permission"),
	NOT_TIRED("not_tired"),
	TOO_TIRED("too_tired"),
	TIRED("tired"),
	VERY_TIRED("very_tired"),
	NO_HAND_CHOP("no_hand_chop"),
	NOT_THIRSTY("not_thirsty"),
	LITTLE_THIRSTY("little_thirsty"),
	GETTING_THIRSTY("getting_thirsty"),
	REALLY_THIRSTY("really_thirsty"),
	COSY("cosy"),
	COLD("cold"),
	HURT("hurt"),
	HUNGRY("hungry"),
	SHOULD_SLEEP("should_sleep"),
	USED_BANDAGE("used_bandage"),
	LEGS_HEALED("legs_healed"),
	BROKEN_BONES("broken_bones"),
	WAYPOINT_SET("waypoint_set");

	@Getter
    private String path;
    
    @Setter
    private static Translator translator;

    private Lang(final String path) {
        this.path = path;
    }
    
    public String getValue(Player player, final String[] args) {
        String value = translator.getTranslationFor(player, this.path);

        if (args == null) {
            return value;
        } else {
            if (args.length == 0)
                return value;

            for (int i = 0; i < args.length; i++) {
                value = value.replace("{" + i + "}", args[i]);
            }
        }

        return value;
    }
}
 
