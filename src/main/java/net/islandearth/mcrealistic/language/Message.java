/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 ******************************************************************************/
package net.islandearth.mcrealistic.language;

import org.bukkit.entity.Player;

import net.islandearth.mcrealistic.utils.TitleManager;

public class Message {
	
	private Player player;
	private MessageType type;
	private Lang lang;
	private String[] variables;
	
	public Message(Player player, MessageType type, Lang lang, String[] variables) {
		this.type = type;
		this.lang = lang;
		this.player = player;
		this.variables = variables;
	}
	
	public void send() {
		if (type == MessageType.ACTIONBAR) {
			TitleManager.sendActionBar(player, lang.getValue(player, variables));
		} else if (type == MessageType.MESSAGE) {
			player.sendMessage(lang.getValue(player, variables));
		} else if (type == MessageType.TITLE) {
			TitleManager.sendTitle(player, "", lang.getValue(player, variables), 120);
		}
	}
}
