package net.islandearth.mcrealistic.version;

import org.bukkit.Bukkit;

import lombok.AllArgsConstructor;
import lombok.Getter;

public class VersionChecker {
	
	@Getter
	public Version currentVersion;
	
	@Getter
	public Version latestVersion;
	
	public VersionChecker() {
		this.latestVersion = Version.values()[0];
	}
	
	/**
	 * Checks the current server version.
	 * @return true if server is up-to-date
	 */
	public boolean checkVersion() {
		for (Version version : Version.values()) {
			if (Bukkit.getVersion().contains(version.getId())) {
				this.currentVersion = version;
				return true;
			}
		}
		
		this.currentVersion = Version.UNSUPPORTED;
		return false;
	}
	
	@AllArgsConstructor
	public enum Version {
		v1_13_R2("1.13.2"),
		v1_13_R1("1.13.1"),
		UNSUPPORTED("Unsupported");
		
		@Getter
		private String id;
	}
}
