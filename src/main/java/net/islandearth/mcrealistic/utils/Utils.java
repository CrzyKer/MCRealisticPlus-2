/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 ******************************************************************************/
package net.islandearth.mcrealistic.utils;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import net.islandearth.mcrealistic.MCRealistic;

public class Utils {
	
	public static List<Block> getNearbyBlocks(Location location, int radius) {
		
		List<Block> blocks = new ArrayList<Block>();
		for (int x = location.getBlockX() - radius; x <= location.getBlockX() + radius; x++) {
			for (int y = location.getBlockY() - radius; y <= location.getBlockY() + radius; y++) {
				for (int z = location.getBlockZ() - radius; z <= location.getBlockZ() + radius; z++) {
					blocks.add(location.getWorld().getBlockAt(x, y, z));
				}
			}
		}
		
		return blocks;
	}
	
	public static boolean isInRegion(Player player) {
		//TODO
		return false;
	}
	
	public static boolean isWorldEnabled(World world) {
		MCRealistic plugin = (MCRealistic) JavaPlugin.getPlugin(MCRealistic.class);
		for (World worlds : plugin.getWorlds()) {
			if (worlds.getName().equals(world.getName())) {
				return true;
			}
		}
		return false;
	}
}
