/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 ******************************************************************************/
package net.islandearth.mcrealistic.placeholders;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

import me.clip.placeholderapi.expansion.PlaceholderExpansion;
import net.islandearth.mcrealistic.MCRealistic;

public class Placeholders extends PlaceholderExpansion {

	MCRealistic plugin;
	
	public Placeholders(MCRealistic plugin)
	{
		this.plugin = plugin;
	}
	
	@Override
	public String onPlaceholderRequest(Player player, String arg1) 
	{
		if(player == null) return "";
		
		switch(arg1.toLowerCase())
		{
			case "thirst":
				return String.valueOf(getConfig().getInt("Players.Thirst." + player.getUniqueId()));
			case "fatigue":
				return String.valueOf(getConfig().getInt("Players.Fatigue." + player.getUniqueId()));
			case "age":
				return String.valueOf(getConfig().getInt(new StringBuilder("Players.RealAge.").append(player.getUniqueId()).toString()));
			case "name":
				return getConfig().getString(new StringBuilder("Players.RealName.").append(player.getUniqueId()).toString());
			case "nickname":
				return player.getDisplayName();
		}
		
		return null;
	}
	
	private FileConfiguration getConfig()
	{
		return plugin.getConfig();
	}

	@Override
	public String getAuthor() {
		return "SamB440";
	}

	@Override
	public String getIdentifier() {
		return "mcrealistic";
	}

	@Override
	public String getPlugin() {
		return null;
	}

	@Override
	public String getVersion() {
		return "2.0.4";
	}
}
