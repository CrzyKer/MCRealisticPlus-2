/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 ******************************************************************************/
package net.islandearth.mcrealistic.ui;

import java.util.List;

import org.bukkit.inventory.ItemStack;

public class ItemUI extends UI {

	public ItemUI(List<ItemStack> items) {
		super((int) roundUp(items.size(), 9), "MCRealistic Items");
		
		
		int slot = 0;
		for(ItemStack item : items)
		{
			setItem(slot, item, player -> {
				player.getInventory().addItem(item);
				player.updateInventory();
			});
			
			slot++;
		}
	}
	
	private static long roundUp(long n, long m)
	{
	    return n >= 0 ? ((n + m - 1) / m) * m : (n / m) * m;
	}
}
