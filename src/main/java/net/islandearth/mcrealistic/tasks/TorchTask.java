/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 ******************************************************************************/
package net.islandearth.mcrealistic.tasks;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import net.islandearth.mcrealistic.MCRealistic;

public class TorchTask implements Runnable {
	
	MCRealistic plugin;
	
	public TorchTask(MCRealistic plugin)
	{
		this.plugin = plugin;
	}

	@Override
	public void run() {
		if(Bukkit.getServer().getOnlinePlayers().size() > 0)
		{
            for(Player p : Bukkit.getOnlinePlayers()) 
            {
                if (!plugin.getBurning().contains(p.getUniqueId())) continue;
                p.setFireTicks(20);
            }
		}
	}
}
