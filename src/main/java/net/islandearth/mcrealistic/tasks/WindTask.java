package net.islandearth.mcrealistic.tasks;

import java.util.Random;

import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.entity.Player;

import lombok.AllArgsConstructor;
import net.islandearth.mcrealistic.MCRealistic;

@AllArgsConstructor
public class WindTask implements Runnable {
	
	private MCRealistic plugin;

	@Override
	public void run() {
		for(Player player : Bukkit.getOnlinePlayers())
		{
			if(playWind(player))
			{
				float windStrength = (float) (((player.getLocation().getY() - 150) / (256 - 150)) * 100);
				player.playSound(player.getLocation(), Sound.ITEM_ELYTRA_FLYING, windStrength, 1.0F);
			}
		}
	}
	
	private boolean playWind(Player player)
	{
	    if(player.getLocation().getY() < 150) return false;
	    if(!plugin.getWorlds().contains(player.getWorld())) return false;
	    if(player.getWorld().getHighestBlockYAt(player.getLocation()) > player.getLocation().getY()) return false;
	    return new Random().nextInt(50) < 25;
	}
}
