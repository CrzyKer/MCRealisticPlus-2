package net.islandearth.mcrealistic.tasks;

import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.World;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import net.islandearth.mcrealistic.MCRealistic;
import net.islandearth.mcrealistic.language.Lang;
import net.islandearth.mcrealistic.language.Message;
import net.islandearth.mcrealistic.language.MessageType;

public class MiscTask implements Runnable {
	
	private MCRealistic plugin;
	private List<World> worlds;
	
	public MiscTask(MCRealistic plugin) {
		this.plugin = plugin;
		this.worlds = plugin.getWorlds();
	}

	@Override
	public void run() {
        for (Player pl : Bukkit.getOnlinePlayers()) {
        	if (worlds.contains(pl.getWorld())) {
            	if (pl.getGameMode() == GameMode.SURVIVAL) {
                    float WeightLeggings;
                    int CurrentFatigue;
                    float WeightCombined;
                    float WeightChestPlate;

                    if (pl.getHealth() < 6.0 && getConfig().getBoolean("Server.Player.DisplayHurtMessage")) {
                        pl.setSprinting(false);
                        pl.setSneaking(true);
                        Message m = new Message(pl, MessageType.valueOf(getConfig().getString("Server.Messages.Type")), Lang.HURT, null);
                        m.send();
                    }

                    if (getConfig().getBoolean("Server.Player.Allow Fatigue")) {
                        if (getConfig().getInt("Players.Fatigue." + pl.getUniqueId()) >= getConfig().getInt("Server.Player.Max Fatigue")) {
        	        		Message m = new Message(pl, MessageType.valueOf(getConfig().getString("Server.Messages.Type")), Lang.VERY_TIRED, null);
        	        		m.send();
                            pl.damage(3.0);
                            pl.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, 1200, 1));
                            pl.addPotionEffect(new PotionEffect(PotionEffectType.BLINDNESS, 80, 2));
                        }
                        
                        if (getConfig().getInt("Players.Fatigue." + pl.getUniqueId()) >= getConfig().getInt("Server.Player.Fatigue Tired Range Min") 
                        		&& getConfig().getInt("Players.Fatigue." + pl.getUniqueId()) <= getConfig().getInt("Server.Player.Fatigue Tired Range Max")) {
        	        		Message m = new Message(pl, MessageType.valueOf(getConfig().getString("Server.Messages.Type")), Lang.TIRED, null);
        	        		m.send();
                            pl.addPotionEffect(new PotionEffect(PotionEffectType.BLINDNESS, 40, 1));
                        }
                    }
                    
                    if (!getConfig().getBoolean("Server.Weather.WeatherAffectsPlayer")) continue;
                    
                    if (pl.getWorld().hasStorm()) {
                        if (pl.getInventory().getBoots() != null 
                        		&& pl.getInventory().getChestplate() != null) {
                            if (!getConfig().getBoolean("Players.BoneBroke." + pl.getUniqueId())) {
                                getConfig().set("Players.DefaultWalkSpeed." + pl.getPlayer().getUniqueId(), 0.2);
                                WeightLeggings = getConfig().getInt("Players.LeggingsWeight." + pl.getUniqueId());
                                WeightChestPlate = getConfig().getInt("Players.ChestplateWeight." + pl.getUniqueId());
                                WeightCombined = WeightLeggings + WeightChestPlate;
                                pl.setWalkSpeed((float)(getConfig().getDouble("Players.DefaultWalkSpeed." + pl.getPlayer().getUniqueId()) - (double)(WeightCombined * 0.01f)));
                                getConfig().set("Players.Iscolds." + pl.getUniqueId(), false);
                                pl.setWalkSpeed((float)(getConfig().getDouble("Players.DefaultWalkSpeed." + pl.getPlayer().getUniqueId()) - (double)(WeightCombined * 0.01f)));
                            }
                        } else if (pl.getInventory().getBoots() == null 
                        		&& pl.getInventory().getChestplate() == null 
                        		&& !getConfig().getBoolean("Players.InTorch." + pl.getUniqueId()) 
                        		&& !getConfig().getBoolean("Players.NearFire." + pl.getUniqueId())) {
        	        		if (pl.getWorld().getHighestBlockYAt(pl.getLocation()) < pl.getLocation().getY()) {
        	        			Message m = new Message(pl, MessageType.valueOf(getConfig().getString("Server.Messages.Type")), Lang.COLD, null);
	        	        		m.send();
	                            CurrentFatigue = getConfig().getInt("Players.Fatigue." + pl.getUniqueId());
	                            getConfig().set("Players.Fatigue." + pl.getUniqueId(), (CurrentFatigue += 10));
	                            getConfig().set("Players.DefaultWalkSpeed." + pl.getPlayer().getUniqueId(), 0.123);
	                            float WeightLeggings2 = getConfig().getInt("Players.LeggingsWeight." + pl.getUniqueId());
	                            float WeightChestPlate2 = getConfig().getInt("Players.ChestplateWeight." + pl.getUniqueId());
	                            float WeightCombined2 = WeightLeggings2 + WeightChestPlate2;
	                            pl.setWalkSpeed((float)(getConfig().getDouble("Players.DefaultWalkSpeed." + pl.getPlayer().getUniqueId()) - (double)(WeightCombined2 * 0.01f)));
	                            getConfig().set("Players.Iscolds." + pl.getUniqueId(), true);
	                            pl.damage(3.0);
        	        		} else {
        	        			getConfig().set("Players.Iscolds." + pl.getUniqueId(), false);
        	        		}
                        }
                    }
                    
                    if (!pl.getWorld().hasStorm() 
                    		&& !getConfig().getBoolean("Players.BoneBroke." + pl.getUniqueId())) {
                        getConfig().set("Players.DefaultWalkSpeed." + pl.getPlayer().getUniqueId(), 0.2);
                        getConfig().set("Players.Iscolds." + pl.getUniqueId(), false);
                        WeightLeggings = getConfig().getInt("Players.LeggingsWeight." + pl.getUniqueId());
                        WeightChestPlate = getConfig().getInt("Players.ChestplateWeight." + pl.getUniqueId());
                        WeightCombined = WeightLeggings + WeightChestPlate;
                        pl.setWalkSpeed((float)(getConfig().getDouble("Players.DefaultWalkSpeed." + pl.getPlayer().getUniqueId()) - (double)(WeightCombined * 0.01f)));
                    }
                    
                    if (!getConfig().getBoolean("Players.Iscolds." + pl.getUniqueId()) 
                    		&& !getConfig().getBoolean("Players.BoneBroke." + pl.getUniqueId())) {
                        getConfig().set("Players.DefaultWalkSpeed." + pl.getPlayer().getUniqueId(), 0.2);
                        WeightLeggings = getConfig().getInt("Players.LeggingsWeight." + pl.getUniqueId());
                        WeightChestPlate = getConfig().getInt("Players.ChestplateWeight." + pl.getUniqueId());
                        WeightCombined = WeightLeggings + WeightChestPlate;
                        pl.setWalkSpeed((float)(getConfig().getDouble("Players.DefaultWalkSpeed." + pl.getPlayer().getUniqueId()) - (double)(WeightCombined * 0.01f)));
                    }
                    
                    if (!getConfig().getBoolean("Players.InTorch." + pl.getUniqueId()) 
                    		|| !pl.getWorld().hasStorm() || getConfig().getBoolean("Players.BoneBroke." + pl.getUniqueId())) continue;
                    
                    getConfig().set("Players.DefaultWalkSpeed." + pl.getPlayer().getUniqueId(), 0.2);
                    WeightLeggings = getConfig().getInt("Players.LeggingsWeight." + pl.getUniqueId());
                    WeightChestPlate = getConfig().getInt("Players.ChestplateWeight." + pl.getUniqueId());
                    WeightCombined = WeightLeggings + WeightChestPlate;
                    pl.setWalkSpeed((float)(getConfig().getDouble("Players.DefaultWalkSpeed." + pl.getPlayer().getUniqueId()) - (double)(WeightCombined * 0.01f)));
	        		Message m = new Message(pl, MessageType.valueOf(getConfig().getString("Server.Messages.Type")), Lang.COSY, null);
	        		m.send();
                    CurrentFatigue = getConfig().getInt("Players.Fatigue." + pl.getUniqueId());
                    getConfig().set("Players.Fatigue." + pl.getUniqueId(), (--CurrentFatigue));
                }
            }
        }
	}
	
	private FileConfiguration getConfig() {
		return plugin.getConfig();
	}

}
