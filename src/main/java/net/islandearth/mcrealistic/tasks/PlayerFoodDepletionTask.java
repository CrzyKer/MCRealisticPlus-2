package net.islandearth.mcrealistic.tasks;

import lombok.Setter;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

class PlayerFoodDepletionTask implements Runnable{
    private Player player;
    private int foodToDeplete;

    @Setter
    private int taskId;

    public PlayerFoodDepletionTask(Player player, int foodToDeplete) {
        this.player = player;
        this.foodToDeplete = foodToDeplete;
    }

    @Override
    public void run() {

        if (foodToDeplete-- == 0) {
            Bukkit.getScheduler().cancelTask(taskId);
            return;
        }
        player.setFoodLevel(player.getFoodLevel() - 1);
    }
}
