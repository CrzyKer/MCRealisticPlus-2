/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 ******************************************************************************/
package net.islandearth.mcrealistic.tasks;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffectType;

import net.islandearth.mcrealistic.MCRealistic;

public class CooldownFixTask implements Runnable {

	private MCRealistic plugin;
	
	public CooldownFixTask(MCRealistic plugin)
	{
		this.plugin = plugin;
	}
	
	@Override
	public void run() {
		for(Player player : Bukkit.getOnlinePlayers())
		{
			if(player.hasMetadata("digging") && player.getTargetBlock(null, 6).getType() == Material.AIR) 
			{
				player.removePotionEffect(PotionEffectType.SLOW_DIGGING);
				player.removeMetadata("digging", plugin);
			}
		}
	}
}
