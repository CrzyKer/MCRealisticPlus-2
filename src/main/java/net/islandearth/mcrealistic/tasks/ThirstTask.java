package net.islandearth.mcrealistic.tasks;

import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.World;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import net.islandearth.mcrealistic.MCRealistic;
import net.islandearth.mcrealistic.language.Lang;
import net.islandearth.mcrealistic.language.Message;
import net.islandearth.mcrealistic.language.MessageType;

public class ThirstTask implements Runnable {
	
	private MCRealistic plugin;
	private List<World> worlds;
	
	public ThirstTask(MCRealistic plugin) {
		this.plugin = plugin;
		this.worlds = plugin.getWorlds();
	}
	
	@Override
	public void run() {
		for (Player pl : Bukkit.getOnlinePlayers()) {
			if (worlds.contains(pl.getWorld())) {
    			if (pl.getGameMode() == GameMode.SURVIVAL) {
    				int CurrentThirst = getConfig().getInt("Players.Thirst." + pl.getUniqueId());
    				if (CurrentThirst <= 100 && CurrentThirst > 0) {
    					getConfig().set("Players.Thirst." + pl.getUniqueId(), (CurrentThirst + 100));
    	        		Message m = new Message(pl, MessageType.valueOf(getConfig().getString("Server.Messages.Type")), Lang.GETTING_THIRSTY, null);
    	        		m.send();
						pl.addPotionEffect((new PotionEffect(PotionEffectType.CONFUSION, 10, 10)));
    				}
    				
    				if (CurrentThirst >= 200) {
    	        		Message m = new Message(pl, MessageType.valueOf(getConfig().getString("Server.Messages.Type")), Lang.REALLY_THIRSTY, null);
    	        		m.send();
						pl.damage(3.0);
						pl.addPotionEffect((new PotionEffect(PotionEffectType.CONFUSION, 10, 10)));
						pl.addPotionEffect((new PotionEffect(PotionEffectType.BLINDNESS, 10, 10)));
						int CurrentFatigue = getConfig().getInt("Players.Fatigue." + pl.getUniqueId());
						getConfig().set("Players.Fatigue." + pl.getUniqueId(), (CurrentFatigue += 20));
    				}
    				
    				if (CurrentThirst != 0) continue;
    				getConfig().set("Players.Thirst." + pl.getUniqueId(), (CurrentThirst + 100));
	        		Message m = new Message(pl, MessageType.valueOf(getConfig().getString("Server.Messages.Type")), Lang.LITTLE_THIRSTY, null);
	        		m.send();
    			}
    		}
    	}
	}
	
	private FileConfiguration getConfig() {
		return plugin.getConfig();
	}
}
