package net.islandearth.mcrealistic.tasks;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import net.islandearth.mcrealistic.MCRealistic;
import net.islandearth.mcrealistic.language.Lang;
import net.islandearth.mcrealistic.language.Message;
import net.islandearth.mcrealistic.language.MessageType;
import net.islandearth.mcrealistic.utils.Utils;

public class CosyTask implements Runnable {

    private MCRealistic plugin;
    private List<World> worlds;
    private Configuration config;
    private ConfigurationSection section;

    private int intervalCounter = 0;

    public CosyTask(MCRealistic plugin) {
        this.plugin = plugin;
        this.worlds = plugin.getWorlds();
    }


    @Override
    public void run() {
        if (plugin.getConfig().getBoolean("Server.Player.Cosy.DisplayMessage")) {
            config = plugin.getConfig();
            section = config.getConfigurationSection("Server.Player.Cosy");
            if (++intervalCounter != section.getInt("CallInterval")) {
                return;
            }
            intervalCounter = 0;
            for (Player player : Bukkit.getOnlinePlayers()) {
                if (worlds.contains(player.getWorld())) {

                    Message messageCosy = new Message(player,
                            MessageType.valueOf(config.getString("Server.Messages.Type")),
                            Lang.COSY, null);
                    config.set("Players.NearFire." + player.getUniqueId(), false);
                    config.set("Players.InTorch." + player.getUniqueId(), false);
                    int furnaceRadius = section.getInt("FurnaceRadius");
                    double torchRadius = section.getDouble("TorchRadius");
                    double inclementWeatherHeatReduction = section.getDouble("InclementWeatherHeatReduction");
                    int highestBlockAtPlayer = player.getWorld().getHighestBlockYAt(player.getLocation());

                    if (player.getWorld().hasStorm() && player.getLocation().getY()>=highestBlockAtPlayer) {
                            furnaceRadius *= inclementWeatherHeatReduction;
                            torchRadius *= inclementWeatherHeatReduction;
                    }

                    for (Block block :
                            Utils.getNearbyBlocks(player.getLocation(), furnaceRadius)) {

                        Material blockType = block.getType();
                        if ((blockType.equals(Material.FIRE)
                                || blockType.equals(Material.FURNACE))
                                || blockType.equals(Material.LAVA)) {

                            if (section.getBoolean("FurnaceMustBeLit")
                                    && blockType.equals(Material.FURNACE)
                                    && ((Furnace) block.getState()).getBurnTime() == 0) {
                                break;
                            }

                            playerIsCosy(player, section.getInt("FurnaceDuration"), messageCosy);

                            config.set("Players.NearFire." + player.getUniqueId(), true);
                            break;

                        } else if (player.getLocation().distance(block.getLocation()) <= torchRadius
                                && (blockType.equals(Material.TORCH) || blockType.equals(Material.WALL_TORCH))) {

                            playerIsCosy(player, section.getInt("TorchDuration"), messageCosy);

                            config.set("Players.InTorch." + player.getUniqueId(), true);
                        }
                    }
                }
            }
        }
    }

    private void playerIsCosy(Player player, int duration, Message message) {
        if (plugin.getColds().contains(player.getUniqueId()) || plugin.getDiseases().contains(player.getUniqueId()))
            return;

        int timeDuration = duration * 60;
        message.send();
        int foodLevel = player.getFoodLevel();
        if (section.getInt("FoodLevelForRegeneration") <= foodLevel) {
            int foodDepletion = section.getInt("FoodUseToRecoverHealth");

            player.addPotionEffect(new PotionEffect(PotionEffectType.REGENERATION, timeDuration, 0));
            if (player.getHealth() < 20) {

                PlayerFoodDepletionTask depletionTask = new PlayerFoodDepletionTask(player,
                        foodDepletion * duration);
                int id = Bukkit.getScheduler().scheduleSyncRepeatingTask(plugin,
                        depletionTask, 0, timeDuration / (2 * foodDepletion));
                depletionTask.setTaskId(id);
            }
        }
    }
}

