package net.islandearth.mcrealistic.tasks;

import java.util.List;
import java.util.Random;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Sound;
import org.bukkit.World;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import net.islandearth.mcrealistic.MCRealistic;
import net.islandearth.mcrealistic.utils.TitleManager;

public class DiseaseTask implements Runnable {
	
	private MCRealistic plugin;
	private List<World> worlds;
	
	public DiseaseTask(MCRealistic plugin) {
		this.plugin = plugin;
		this.worlds = plugin.getWorlds();
	}
	
	@Override
	public void run() {
		if (Bukkit.getOnlinePlayers().size() >= getConfig().getInt("Server.Player.Immune_System.Req_Players")) {
			int random = new Random().nextInt(Bukkit.getServer().getOnlinePlayers().size());
			Player player = (Player) Bukkit.getServer().getOnlinePlayers().toArray()[random];
			
			if (worlds.contains(player.getWorld())) {
				if (player.hasPermission("mcr.getcolds") && plugin.getDiseases().contains(player.getUniqueId()) && player.getGameMode() == GameMode.SURVIVAL) {
					TitleManager.sendTitle(player, "", ChatColor.RED + "The disease begins to damage your body...", 200);
					player.playSound(player.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 1.0F, 1.0F);
					player.damage(4);
					player.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, 600, 0));
					player.addPotionEffect(new PotionEffect(PotionEffectType.CONFUSION, 200, 0));
					player.addPotionEffect(new PotionEffect(PotionEffectType.POISON, 100, 0));
    			} else if(player.hasPermission("mcr.getcolds") && plugin.getColds().contains(player.getUniqueId()) && player.getGameMode() == GameMode.SURVIVAL) {
					plugin.getColds().remove(player.getUniqueId());
					plugin.getDiseases().add(player.getUniqueId());
					TitleManager.sendTitle(player, "", ChatColor.RED + "Your colds developed into a disease!", 200);
					player.playSound(player.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 1.0F, 1.0F);
					player.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, 600, 0));
					player.addPotionEffect(new PotionEffect(PotionEffectType.CONFUSION, 200, 0));
					TitleManager.sendActionBar(player, ChatColor.GREEN + "" + ChatColor.BOLD + "TIP:" + ChatColor.WHITE + " Use medicine to fight the disease!");
				} else if(player.hasPermission("mcr.getcolds") && !plugin.getColds().contains(player.getUniqueId()) && player.getGameMode() == GameMode.SURVIVAL) {
					plugin.getColds().add(player.getUniqueId());
					TitleManager.sendTitle(player, "", ChatColor.RED + "You have caught a cold!", 200);
					player.playSound(player.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 1.0F, 1.0F);
					player.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, 3000, 0));
					player.damage(2);
					TitleManager.sendActionBar(player, ChatColor.GREEN + "" + ChatColor.BOLD + "TIP:" + ChatColor.WHITE + " Use medicine to fight the colds!");
				}
			}
		}
	}

	private FileConfiguration getConfig() {
		return plugin.getConfig();
	}
}
