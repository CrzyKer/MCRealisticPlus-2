package net.islandearth.mcrealistic;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import java.util.logging.Logger;

import org.apache.commons.lang.StringUtils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.NamespacedKey;
import org.bukkit.Sound;
import org.bukkit.World;
import org.bukkit.command.CommandMap;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.craftbukkit.libs.org.apache.commons.codec.binary.Base64;
import org.bukkit.entity.Player;
import org.bukkit.inventory.FurnaceRecipe;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.ShapedRecipe;
import org.bukkit.inventory.ShapelessRecipe;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.PotionMeta;
import org.bukkit.inventory.meta.SkullMeta;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.potion.PotionData;
import org.bukkit.potion.PotionType;

import com.mojang.authlib.GameProfile;
import com.mojang.authlib.properties.Property;
import com.sk89q.worldguard.WorldGuard;

import lombok.Getter;
import net.islandearth.languagy.language.Language;
import net.islandearth.languagy.language.Translator;
import net.islandearth.mcrealistic.commands.Fatigue;
import net.islandearth.mcrealistic.commands.MCRealistic_Command;
import net.islandearth.mcrealistic.commands.MyStats;
import net.islandearth.mcrealistic.commands.Thirst;
import net.islandearth.mcrealistic.language.Lang;
import net.islandearth.mcrealistic.listeners.BlockListener;
import net.islandearth.mcrealistic.listeners.ConsumeListener;
import net.islandearth.mcrealistic.listeners.EntityListener;
import net.islandearth.mcrealistic.listeners.FoodChangeListener;
import net.islandearth.mcrealistic.listeners.InteractListener;
import net.islandearth.mcrealistic.listeners.InventoryListener;
import net.islandearth.mcrealistic.listeners.JoinListener;
import net.islandearth.mcrealistic.listeners.MoveListener;
import net.islandearth.mcrealistic.listeners.ProjectileListener;
import net.islandearth.mcrealistic.listeners.RespawnListener;
import net.islandearth.mcrealistic.metrics.Metrics;
import net.islandearth.mcrealistic.placeholders.Placeholders;
import net.islandearth.mcrealistic.tasks.CooldownFixTask;
import net.islandearth.mcrealistic.tasks.CosyTask;
import net.islandearth.mcrealistic.tasks.DiseaseTask;
import net.islandearth.mcrealistic.tasks.MiscTask;
import net.islandearth.mcrealistic.tasks.StaminaTask;
import net.islandearth.mcrealistic.tasks.ThirstTask;
import net.islandearth.mcrealistic.tasks.TorchTask;
import net.islandearth.mcrealistic.tasks.WindTask;
import net.islandearth.mcrealistic.version.VersionChecker;
import net.islandearth.mcrealistic.version.VersionChecker.Version;
import net.md_5.bungee.api.ChatMessageType;
import net.md_5.bungee.api.chat.TextComponent;

public class MCRealistic extends JavaPlugin {
	
	@Getter
	private Translator translator;
	
	@Getter
	private List<World> worlds = new ArrayList<>();
	
	@Getter
	private WorldGuard worldGuard;
	
	@Getter
	private Logger log = Bukkit.getLogger();
	
	@Getter
	private List<String> firstNames = new ArrayList<>();
	
	@Getter
	private List<String> lastNames = new ArrayList<>();
	
	@Getter 
	private List<UUID> colds = new ArrayList<>();
	
	@Getter 
	private List<UUID> diseases = new ArrayList<>();
	
	@Getter
	private List<UUID> burning = new ArrayList<UUID>();
	
	@Getter
	private VersionChecker version;
	
	@Override
	public void onEnable() {
		
		boolean updated = removeUpdater();
		if (!checkVersion()) {
			Bukkit.getPluginManager().disablePlugin(this);
			return;
		}
		
		createFiles();
		hookPlugins();
		registerListeners();
		registerCommands();
		if (!updated) registerRecipes();
		startTasks();
		
        firstNames.add("Mark");
        firstNames.add("Daniel");
        firstNames.add("Ilan");
        firstNames.add("Alex");
        firstNames.add("James");
        firstNames.add("John");
        firstNames.add("Robert");
        firstNames.add("Michael");
        firstNames.add("William");
        firstNames.add("Joseph");
        firstNames.add("Donald");
        firstNames.add("Steven");
        firstNames.add("Kevin");
        firstNames.add("Joe");
        firstNames.add("Carl");
        firstNames.add("Patrick");
        firstNames.add("Peter");
        firstNames.add("Justin");
        firstNames.add("Harry");
        firstNames.add("Howard");
        
        lastNames.add("Hawking");
        lastNames.add("Potter");
        lastNames.add("Biber");
        lastNames.add("Duck");
        lastNames.add("Smith");
        lastNames.add("Brown");
        lastNames.add("White");
        lastNames.add("Lopez");
        lastNames.add("Lee");
        lastNames.add("Thompson");
        lastNames.add("Phillips");
        lastNames.add("Evans");
        lastNames.add("Young");
        lastNames.add("Hall");
        lastNames.add("Cooper");
        lastNames.add("Bell");
        lastNames.add("Morris");
        lastNames.add("Edwards");
        lastNames.add("Baker");
        lastNames.add("Kelly");
        
		@SuppressWarnings("unused")
		Metrics metrics = new Metrics(this);
		
        for (String s : getConfig().getStringList("Worlds")) {
        	World w = Bukkit.getWorld(s);
        	worlds.add(w);
        }
	}
	
	@Override
	public void onDisable() {
		log.info("Disabling...");
		this.translator = null;
		this.version = null;
		this.worlds = new ArrayList<>();
		this.worldGuard = null;
		this.firstNames = new ArrayList<>();
		this.lastNames = new ArrayList<>();
		this.colds = new ArrayList<>();
		this.diseases = new ArrayList<>();
		this.burning = new ArrayList<>();
	}
	
	private void createFiles() {
		createConfig();
		
		File lang = new File(getDataFolder() + "/lang/");
		if (!lang.exists()) lang.mkdirs();
		
		File fallback = new File(getDataFolder() + "/lang/" + "en_gb.yml");
		for (Language language : Language.values()) {
			File file = new File(getDataFolder() + "/lang/" + language.getCode() + ".yml");
			if (!file.exists()) {
			   try {
				   if (language == Language.CZECH) {
					   log.info("Saving default language: " + language.toString());
					   saveResource("lang/cs_cz.yml", false);
				   } else {
				       file.createNewFile();
					   
				       FileConfiguration config = YamlConfiguration.loadConfiguration(file);
				       config.options().copyDefaults(true);
				       config.addDefault("no_permission", "&cYou do not have the required permission {0}");
				       config.addDefault("not_tired", "&aI don't feel tired anymore...");
				       config.addDefault("too_tired", "&cI'm too tired to do that.");
				       config.addDefault("tired", "&cI am tired...");
				       config.addDefault("very_tired", "&cI am very tired... I should get some sleep.");
				       config.addDefault("no_hand_chop", "&cYou can't chop down trees with your hands!");
				       config.addDefault("not_thirsty", "&aI'm not thirsty anymore!");
				       config.addDefault("little_thirsty", "I am a little thirsty...");
				       config.addDefault("getting_thirsty", "&cI am getting thirsty...");
				       config.addDefault("really_thirsty", "&c&l&nI am really thirsty... I should drink some water.");
				       config.addDefault("cosy", "&2I feel cosy...");
				       config.addDefault("cold", "&c&nI am cold, I should wear some clothes (Armour).");
				       config.addDefault("hurt", "&c&lI am hurt!");
				       config.addDefault("hungry", "&c&lI am hungry! I should really eat something...");
				       config.addDefault("should_sleep", "&cI should sleep in that bed...");
				       config.addDefault("used_bandage", "&aYou used a bandage, and your legs healed!");
				       config.addDefault("legs_healed", "&aYour legs healed!");
				       config.addDefault("broken_bones", "&cYou fell from a high place and broke your bones!");
				       config.addDefault("waypoint_set", "&aA waypoint has been set at your current location!");
				       
				       try {
				    	   config.save(file);
				       } catch (IOException e) {
				    	   e.printStackTrace();
				       }
				   }
			   } catch (IOException e) {
			       e.printStackTrace();
			   }
			}
		}
		
		this.translator = new Translator(this, fallback);
		translator.setDisplay(Material.WOODEN_AXE);
		Lang.setTranslator(translator);
	}
	
	private void createConfig() {
    	File file = new File("plugins/MCRealistic-2/config.yml");
    	if (!file.exists()) {
    		
			List<String> worlds = new ArrayList<>();
			worlds.add("world");
			worlds.add("world_Damocles");
			
			getConfig().options().copyDefaults(true);
			
			String header;
			String eol = System.getProperty("line.separator");
			header = "MCRealistic-2 Properties:" + eol;
			header += eol;
			header += "Worlds" + eol;
			header += "  Here you define which worlds you enable." + eol;
			header += eol;
			header += "WeatherAffectsPlayer" + eol;
			header += "  This option defines whether the player should be affected by the weather. Default true." + eol;
			header += eol;
			header += "Thirst" + eol;
			header += "  This option defines whether thirst is enabled. Default true." + eol;
			header += eol;
			header += "DisplayHungerMessage" + eol;
			header += "  Whether the hunger message should be shown. Default true." + eol;
			header += eol;
			header += "Cosy" + eol;
			header += "  DisplayMessage" + eol;
			header += "  Whether the cozy message should be shown. Default true." + eol;
			header += eol;
			header += "  CallInterval" + eol;
			header += "    Must be greater than 0" + eol;
			header += "    A value of 1 = 30 seconds, 2 = 60 seconds, ect..." + eol;
			header += eol;
			header += "  FoodLevelForRegeneration" + eol;
			header += "    Minimum food level to regenerate health. 0 - 20" + eol;
			header += eol;
			header += "  FoodUseToRecoverHealth" + eol;
			header += "    Food consumed in recovering heart 0 - 20" + eol;
			header += eol;
			header += "  TorchRadius" + eol;
			header += "    Distance from torch to feel \"Cosy\"" + eol;
			header += eol;
			header += "  TorchDuration" + eol;
			header += "    1 = 1/2 a heart recovery" + eol;
			header += eol;
			header += "  FurnaceRadius" + eol;
			header += "    Distance from furnace to feel \"Cosy\"" + eol;
			header += eol;
			header += "  FurnaceDuration" + eol;
			header += "    2 = 1 heart recovery" + eol;
			header += eol;
			header += "  FurnaceMustBeLit" + eol;
			header += "    The furnace has to be lit to feel \"Cosy\"" + eol;
			header += eol;
			header += "  InclementWeatherHeatReduction" + eol;
			header += "    % of distance to heat to feel \"Cosy\" in rain/snow. 0.0 - 1.0" + eol;
			header += "    0.3 = 30% of TorchRadius/FurnaceRadius" + eol;

			header += eol;
			header += "DisplayHurtMessage" + eol;
			header += "  Whether the hurt message would be shown. Default true." + eol;
			header += eol;
			header += "Weight" + eol;
			header += "  This option defines whether the player should be affected by weight." + eol;
			header += eol;
			header += "Realistic_Building" + eol;
			header += "  This option defines whether blocks will fall." + eol;
			header += eol;
			header += "Messages.Type" + eol;
			header += "  MESSAGE, ACTIONBAR, TITLE" + eol;
			header += eol;
			getConfig().options().header(header);
			
			getConfig().addDefault("Worlds", worlds);
			getConfig().addDefault("Server.Weather.WeatherAffectsPlayer", true);
			getConfig().addDefault("Server.Player.Thirst", true);
			getConfig().addDefault("Server.Player.Wind", true);
			getConfig().addDefault("Server.World.Falling_Trees", true);
			getConfig().addDefault("Server.Stamina.Enabled", true);
			getConfig().addDefault("Server.Player.DisplayHungerMessage", true);
			getConfig().addDefault("Server.Player.Cosy.DisplayMessage", true);
			getConfig().addDefault("Server.Player.Cosy.CallInterval", 1);
			getConfig().addDefault("Server.Player.Cosy.FoodLevelForRegeneration", 19);
			getConfig().addDefault("Server.Player.Cosy.FoodUseToRecoverHealth", 3);
			getConfig().addDefault("Server.Player.Cosy.TorchRadius", 1.75);
			getConfig().addDefault("Server.Player.Cosy.TorchDuration", 1);
			getConfig().addDefault("Server.Player.Cosy.FurnaceRadius", 4);
			getConfig().addDefault("Server.Player.Cosy.FurnaceDuration", 2);
			getConfig().addDefault("Server.Player.Cosy.FurnaceMustBeLit", true);
			getConfig().addDefault("Server.Player.Cosy.InclementWeatherHeatReduction", 0.5f);
			getConfig().addDefault("Server.Player.DisplayHurtMessage", true);
			getConfig().addDefault("Server.Player.Weight", true);
			getConfig().addDefault("Server.Player.Broken_Bones.Enabled", true);
			getConfig().addDefault("Server.Building.Realistic_Building", true);
			getConfig().addDefault("Server.Building.Ignored_Blocks", Arrays.asList(
					"TORCH",
					"WALL_TORCH",
					"REDSTONE_WALL_TORCH", 
					"REDSTONE_TORCH", 
					
					"SIGN", 
					"WALL_SIGN", 
					
					"VINE",
					"LADDER",
					
					"OAK_BUTTON",
					"STONE_BUTTON",
					
					"OAK_FENCE",
					"BIRCH_FENCE",
					"SPRUCE_FENCE",
					"JUNGLE_FENCE",
					"ACACIA_FENCE",
					"NETHER_BRICK_FENCE",
					"DARK_OAK_FENCE",
					
					"OAK_SLAB",
					"ACACIA_SLAB",
					"SPRUCE_SLAB",
					"BIRCH_SLAB",
					"JUNGLE_SLAB",
					"COBBLESTONE_SLAB",
					"DARK_OAK_SLAB",
					"DARK_PRISMARINE_SLAB",
					"NETHER_BRICK_SLAB",
					"PRISMARINE_BRICK_SLAB",
					"PRISMARINE_SLAB",
					"PETRIFIED_OAK_SLAB",
					"PURPUR_SLAB",
					"QUARTZ_SLAB",
					"RED_SANDSTONE_SLAB",
					"SANDSTONE_SLAB",
					"STONE_BRICK_SLAB",
					"STONE_SLAB",
					
					"ACACIA_LOG",
					"OAK_LOG",
					"SPRUCE_LOG",
					"BIRCH_LOG",
					"JUNGLE_LOG",
					"STRIPPED_ACACIA_LOG",
					"STRIPPED_OAK_LOG",
					"STRIPPED_SPRUCE_LOG",
					"STRIPPED_BIRCH_LOG",
					"STRIPPED_JUNGLE_LOG"));
			getConfig().addDefault("Server.Player.Trail.Grass_Blocks", Arrays.asList(
					"DIRT"));
			getConfig().addDefault("Server.Player.Trail.Sand_Blocks", Arrays.asList(
					"SANDSTONE"));
			getConfig().addDefault("Server.Player.Trail.Dirt_Blocks", Arrays.asList(
					"GRASS_PATH"));
			getConfig().addDefault("Server.Player.Trail.Path_Blocks", Arrays.asList(
					"COBBLESTONE"));
			getConfig().addDefault("Server.Player.Trail.Enabled", true);
			getConfig().addDefault("Server.Player.Path", true);
			getConfig().addDefault("Server.Player.Allow Fatigue", true);
			getConfig().addDefault("Server.Player.Max Fatigue", 240);
			getConfig().addDefault("Server.Player.Fatigue Tired Range Min", 150);
			getConfig().addDefault("Server.Player.Fatigue Tired Range Max", 200);
			getConfig().addDefault("Server.Player.Allow Chop Down Trees With Hands", false);
			getConfig().addDefault("Server.Player.Trees have random number of drops", true);
			getConfig().addDefault("Server.Player.Allow /mystats", true);
			getConfig().addDefault("Server.Player.Allow /fatigue", true);
			getConfig().addDefault("Server.Player.Allow /thirst", true);
			getConfig().addDefault("Server.Player.Spawn with items", true);
			getConfig().addDefault("Server.Player.Allow Enchanted Arrow", true);
			getConfig().addDefault("Server.Player.Torch_Burn", true);
			getConfig().addDefault("Server.Messages.Type", "MESSAGE");
    		getConfig().addDefault("Server.Messages.Respawn", true);
    		getConfig().addDefault("Server.Player.Thirst.Interval", 6000);
    		getConfig().addDefault("Server.Player.Thirst.Enabled", true);
    		getConfig().addDefault("Server.Player.Immune_System.Interval", 6000);
    		getConfig().addDefault("Server.Player.Immune_System.Enabled", true);
    		getConfig().addDefault("Server.Player.Immune_System.Req_Players", 2);
    		getConfig().addDefault("Server.GameMode.Type", "NORMAL");
    		saveConfig();
    	}
	}
	
	private void hookPlugins() {
		if (Bukkit.getPluginManager().getPlugin("WorldGuard") != null)  {
			log.info("WorldGuard found!");
			this.worldGuard = WorldGuard.getInstance();
		} else {
			log.severe("WorldGuard not found, disabling!");
			Bukkit.getServer().getPluginManager().disablePlugin(this);
		}
		
		if (Bukkit.getPluginManager().getPlugin("Languagy") != null) {
			log.info("Languagy found!");
		} else {
			log.severe("Languagy not found, disabling!");
			Bukkit.getServer().getPluginManager().disablePlugin(this);
		}
		
		if (Bukkit.getPluginManager().getPlugin("PlaceholderAPI") != null) {
			log.info("PlaceholderAPI found!");
			new Placeholders(this).register();
		} else {
			log.info("PlaceholderAPI not found!");
		}
	}
	
	private void registerListeners() {
		PluginManager pm = Bukkit.getPluginManager();
		pm.registerEvents(new InteractListener(this), this);
		pm.registerEvents(new JoinListener(this), this);
		pm.registerEvents(new RespawnListener(this), this);
		pm.registerEvents(new ProjectileListener(this), this);
		
		List<Material> ignore = new ArrayList<Material>();
		for (String s : getConfig().getStringList("Server.Building.Ignored_Blocks")) {
			Material m = Material.valueOf(s);
			ignore.add(m);
		}
		
		pm.registerEvents(new BlockListener(this, ignore), this);
		pm.registerEvents(new MoveListener(this), this);
		pm.registerEvents(new ConsumeListener(this), this);
		pm.registerEvents(new InventoryListener(this), this);
		pm.registerEvents(new FoodChangeListener(this), this);
		pm.registerEvents(new EntityListener(this), this);
	}
	
	private void registerCommands() {
		try {
			Field bukkitCommandMap = Bukkit.getServer().getClass().getDeclaredField("commandMap");
			bukkitCommandMap.setAccessible(true);
			CommandMap commandMap = (CommandMap) bukkitCommandMap.get(Bukkit.getServer());

			commandMap.register("Thirst", new Thirst(this));
			commandMap.register("Fatigue", new Fatigue(this));
			commandMap.register("MCRealistic", new MCRealistic_Command(this));
			commandMap.register("MyStats", new MyStats(this));
		} catch (NoSuchFieldException | 
				SecurityException | 
				IllegalArgumentException | 
				IllegalAccessException e) {
			e.printStackTrace();
		}
	}
	
	private void registerRecipes() {
		ItemStack medicine = new ItemStack(Material.POTION, 2);
		ItemMeta medicinemeta = medicine.getItemMeta();
		medicinemeta.setDisplayName(ChatColor.GREEN + "Medicine");
		medicinemeta.setLore(Arrays.asList(ChatColor.WHITE + "Drink to help fight your cold/disease!"));
		medicine.setItemMeta(medicinemeta);
		
		ShapelessRecipe medicinecraft = new ShapelessRecipe(new NamespacedKey(this, getDescription().getName() + "-medicine"), medicine);
		medicinecraft.addIngredient(Material.GLASS_BOTTLE);
		medicinecraft.addIngredient(Material.APPLE);
		medicinecraft.addIngredient(Material.SPIDER_EYE);
		Bukkit.getServer().addRecipe(medicinecraft);
		
		ItemStack chocolatemilk = new ItemStack(Material.MILK_BUCKET);
		ItemMeta chocolatemilkmeta = chocolatemilk.getItemMeta();
		chocolatemilkmeta.setDisplayName(ChatColor.DARK_GRAY + "" + ChatColor.BOLD + "Chocolate Milk");
		chocolatemilkmeta.setLore(Arrays.asList(ChatColor.WHITE + "Drink to gain Speed II."));
		chocolatemilk.setItemMeta(chocolatemilkmeta);
		
		ShapedRecipe chocolatemilkcraft = new ShapedRecipe(new NamespacedKey(this, getDescription().getName() + "-chocolate_milk"), chocolatemilk);
			chocolatemilkcraft.shape(
			    "   ",
			    "CBC",
			    "   ");
			
		chocolatemilkcraft.setIngredient('C', Material.COCOA_BEANS);
		chocolatemilkcraft.setIngredient('B', Material.BUCKET);
		Bukkit.getServer().addRecipe(chocolatemilkcraft);
		
		ItemStack bandage = new ItemStack(Material.PAPER);
		ItemMeta bm = bandage.getItemMeta();
		bm.setDisplayName(ChatColor.DARK_AQUA + "Bandage");
	    bandage.setItemMeta(bm);
	    
	    ShapelessRecipe BandageRecipe = new ShapelessRecipe(new NamespacedKey(this, getDescription().getName() + "-bandage"), bandage);
	    BandageRecipe.addIngredient(Material.BONE_MEAL);
	    BandageRecipe.addIngredient(Material.PAPER);
	    Bukkit.getServer().addRecipe(BandageRecipe);
	    
	    ItemStack chocolate = getSkull("http://textures.minecraft.net/texture/a829b238f2ca2f39f2ce43e316e88e8ddfed524a5dfffefafbe8161dd9f93e9");
	    ItemMeta cm = chocolate.getItemMeta();
	    cm.setDisplayName("Chocolate");
	    chocolate.setItemMeta(cm);
	    
	    ShapedRecipe chocolateRecipe = new ShapedRecipe(new NamespacedKey(this, getDescription().getName() + "-chocolate"), chocolate);
	    chocolateRecipe.shape(
	    		" S ",
	    		" C ",
	    		" S ");
	    
	    chocolateRecipe.setIngredient('S', Material.SUGAR);
	    chocolateRecipe.setIngredient('C', Material.COCOA);
	    Bukkit.getServer().addRecipe(chocolateRecipe);
	    
	    ItemStack milkchocolate = getSkull("http://textures.minecraft.net/texture/a829b238f2ca2f39f2ce43e316e88e8ddfed524a5dfffefafbe8161dd9f93e9");
	    ItemMeta mcm = milkchocolate.getItemMeta();
	    mcm.setDisplayName("Milk Chocolate");
	    milkchocolate.setItemMeta(mcm);
	    
	    ShapedRecipe milkChocolateRecipe = new ShapedRecipe(new NamespacedKey(this, getDescription().getName() + "-milk_chocolate"), milkchocolate);
	    milkChocolateRecipe.shape(
	    		" S ",
	    		"MCM",
	    		" S ");
	    
	    milkChocolateRecipe.setIngredient('S', Material.SUGAR);
	    milkChocolateRecipe.setIngredient('C', Material.COCOA);
	    milkChocolateRecipe.setIngredient('M', Material.MILK_BUCKET);
	    Bukkit.getServer().addRecipe(milkChocolateRecipe);
	    
	    ShapelessRecipe axe = new ShapelessRecipe(new NamespacedKey(this, getDescription().getName() + "-axe"), new ItemStack(Material.WOODEN_AXE));
	    axe.addIngredient(Material.STICK);
	    axe.addIngredient(Material.FLINT);
	    axe.addIngredient(Material.FLINT);
	    Bukkit.getServer().addRecipe(axe);
	    
	    ItemStack water = new ItemStack(Material.POTION);
	    PotionMeta wm = (PotionMeta) water.getItemMeta();
	    wm.setBasePotionData(new PotionData(PotionType.WATER));
	    wm.setLore(Arrays.asList(ChatColor.GRAY + "Purified"));
	    water.setItemMeta(wm);
	    
	    FurnaceRecipe purified = new FurnaceRecipe(new NamespacedKey(this, getDescription().getName() + "-purified"), water, Material.POTION, 1, 150);
	    Bukkit.getServer().addRecipe(purified);
	}
	
	private void startTasks() {
		if (getConfig().getBoolean("Server.Stamina.Enabled")) {
			Bukkit.getScheduler().scheduleSyncRepeatingTask(this, new StaminaTask(this), 0L, 60L);
		}
		
		if (getConfig().getBoolean("Server.Player.Torch_Burn")) {
			Bukkit.getScheduler().scheduleSyncRepeatingTask(this, new TorchTask(this), 0L, 20L);
		}
		
		if (getConfig().getBoolean("Server.Player.Wind")) {
			Bukkit.getScheduler().scheduleSyncRepeatingTask(this, new WindTask(this), 0L, 100L);
		}
		
		if (getConfig().getBoolean("Server.Player.Allow Fatigue")) {
			Bukkit.getScheduler().scheduleSyncRepeatingTask(this, new CosyTask(this), 0L, 600L);
		}
		
		if (getConfig().getBoolean("Server.Player.Thirst.Enabled")) {
			Bukkit.getScheduler().scheduleSyncRepeatingTask(this, new ThirstTask(this), 0L, getConfig().getInt("Server.Player.Thirst.Interval"));
		}
		
		if (getConfig().getBoolean("Server.Player.Immune_System.Enabled")) {
			Bukkit.getScheduler().scheduleSyncRepeatingTask(this, new DiseaseTask(this), 0L, getConfig().getInt("Server.Player.Immune_System.Interval"));
		}
		
		Bukkit.getScheduler().scheduleSyncRepeatingTask(this, new MiscTask(this), 0L, 400L);
		Bukkit.getScheduler().scheduleSyncRepeatingTask(this, new CooldownFixTask(this), 0L, 60L);
	}
	
	public ItemStack getSkull(String url)  {
        ItemStack head = new ItemStack(Material.PLAYER_HEAD, 1);
        if (url.isEmpty()) return head;
        
        Bukkit.getScheduler().runTaskAsynchronously(this, () -> {
        	SkullMeta headMeta = (SkullMeta) head.getItemMeta();
        	GameProfile profile = new GameProfile(UUID.randomUUID(), null);
        	byte[] encodedData = Base64.encodeBase64(String.format("{textures:{SKIN:{url:\"%s\"}}}", url).getBytes());
        	profile.getProperties().put("textures", new Property("textures", new String(encodedData)));
        	Field profileField = null;
        	
        	try {
        		profileField = headMeta.getClass().getDeclaredField("profile");
        		profileField.setAccessible(true);
        		profileField.set(headMeta, profile);
        	} catch (NoSuchFieldException | IllegalArgumentException | IllegalAccessException e1) {
        		e1.printStackTrace();
        	}
             
        	head.setItemMeta(headMeta);
        });
        
        return head;
	}
	
	private boolean removeUpdater() {
		File updater = new File("plugins/UpdaterDummy.jar");
		Bukkit.getScheduler().runTaskLaterAsynchronously(this, () -> {
			if (updater.exists()) {
				log.warning("Removing updater plugin!");
				sendActionBar(ChatColor.YELLOW + "Removing updater plugin!");
				sendMessage(ChatColor.GREEN + "It seems you updated MCRealistic recently, but a full restart is recommended to prevent any problems.");
				try {
					updater.delete();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}, 40L);
		return updater.exists();
	}
	
	private boolean checkVersion() {
		this.version = new VersionChecker();
		List<String> supported = new ArrayList<>();
		for (Version version : Version.values()) {
			if (version != Version.UNSUPPORTED) {
				supported.add(version.getId());
			}
		}
		
		if (!version.checkVersion()) {
			log.info(" ");
			log.info(ChatColor.RED + "You are using an unsupported version!");
			log.info(ChatColor.RED + "Your current version is: " + version.getCurrentVersion().getId());
			log.info(ChatColor.RED + "The latest version is: " + version.getLatestVersion().getId());
			log.info(ChatColor.GREEN + "This plugin supports: " + StringUtils.join(supported, ','));
			log.info(" ");
			return false;
		}
		
		log.info(" ");
		log.info(ChatColor.GREEN + "You are running version " + version.getCurrentVersion().getId() + ".");
		log.info(" ");
		return true;
	}
	
	private void sendActionBar(String message) {
		for (Player admin : Bukkit.getOnlinePlayers()) {
			if (admin.isOp()) {
				admin.spigot().sendMessage(ChatMessageType.ACTION_BAR, TextComponent.fromLegacyText(message));
			}
		}
	}
	
	private void sendMessage(String message) {
		for (Player admin : Bukkit.getOnlinePlayers()) {
			if (admin.isOp()) {
				admin.playSound(admin.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
				admin.sendMessage(message);
			}
		}
	}
}
