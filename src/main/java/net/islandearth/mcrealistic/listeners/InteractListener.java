/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 ******************************************************************************/
package net.islandearth.mcrealistic.listeners;

import java.util.Arrays;
import java.util.List;
import java.util.Set;

import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.Tag;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.craftbukkit.v1_14_R1.CraftWorld;
import org.bukkit.craftbukkit.v1_14_R1.inventory.CraftItemStack;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import net.islandearth.mcrealistic.MCRealistic;
import net.islandearth.mcrealistic.gamemode.CustomGameMode;
import net.islandearth.mcrealistic.language.Lang;
import net.islandearth.mcrealistic.language.Message;
import net.islandearth.mcrealistic.language.MessageType;
import net.minecraft.server.v1_14_R1.BlockPosition;
import net.minecraft.server.v1_14_R1.IBlockData;

public class InteractListener implements Listener {
	
	private MCRealistic plugin;
	private List<World> worlds;
    private ItemStack bandage;
    private Set<Material> leaves;
    private List<Material> beds;
    private List<Material> ignore;
	
	public InteractListener(MCRealistic plugin)
	{
		this.plugin = plugin;
		this.worlds = plugin.getWorlds();
		ItemStack bandage = new ItemStack(Material.PAPER);
		ItemMeta bm = bandage.getItemMeta();
		bm.setDisplayName(ChatColor.DARK_AQUA + "Bandage");
	    bandage.setItemMeta(bm);
		this.bandage = bandage;
		this.beds = Arrays.asList(Material.BLACK_BED, 
				Material.BLUE_BED, 
				Material.BROWN_BED, 
				Material.CYAN_BED, 
				Material.GRAY_BED, 
				Material.GREEN_BED,
				Material.LIGHT_BLUE_BED,
				Material.LIGHT_GRAY_BED,
				Material.LIME_BED,
				Material.MAGENTA_BED,
				Material.ORANGE_BED,
				Material.PINK_BED,
				Material.PURPLE_BED,
				Material.RED_BED,
				Material.WHITE_BED,
				Material.YELLOW_BED);
		this.leaves = Tag.LEAVES.getValues();
		this.ignore = Arrays.asList(Material.GRASS,
				Material.TALL_GRASS,
				Material.SEAGRASS,
				Material.TALL_SEAGRASS,
				Material.FLOWER_POT,
				Material.SUNFLOWER,
				Material.CHORUS_FLOWER,
				Material.OXEYE_DAISY,
				Material.DEAD_BUSH,
				Material.FERN,
				Material.DANDELION,
				Material.POPPY,
				Material.BLUE_ORCHID,
				Material.ALLIUM,
				Material.AZURE_BLUET,
				Material.RED_TULIP,
				Material.ORANGE_TULIP,
				Material.WHITE_TULIP,
				Material.PINK_TULIP,
				Material.BROWN_MUSHROOM,
				Material.RED_MUSHROOM,
				Material.END_ROD,
				Material.ROSE_BUSH,
				Material.PEONY,
				Material.LARGE_FERN,
				Material.REDSTONE,
				Material.REPEATER,
				Material.COMPARATOR,
				Material.LEVER,
				Material.SEA_PICKLE,
				Material.SUGAR_CANE,
				Material.FIRE,
				Material.WHEAT,
				Material.WHEAT_SEEDS,
				Material.CARROTS,
				Material.BEETROOT,
				Material.BEETROOT_SEEDS,
				Material.MELON,
				Material.MELON_STEM,
				Material.MELON_SEEDS,
				Material.POTATOES,
				Material.PUMPKIN,
				Material.PUMPKIN_STEM,
				Material.PUMPKIN_SEEDS);
	}
	
	@EventHandler(priority = EventPriority.LOW)
	public void onInteract(PlayerInteractEvent pie) {
		Player player = pie.getPlayer();
		CustomGameMode gamemode = CustomGameMode.valueOf(getConfig().getString("Server.GameMode.Type").toUpperCase());
		if (worlds.contains(player.getWorld())) {
			if (gamemode == CustomGameMode.PRE_ADVENTURE 
					&& pie.getAction() == Action.LEFT_CLICK_BLOCK
					&& player.getGameMode() == GameMode.SURVIVAL) {
				Block clicked = pie.getClickedBlock();
				if (!leaves.contains(clicked.getType()) && !ignore.contains(clicked.getType())) {
				    
					if (clicked.getType() == Material.GRAVEL) {
						if (player.hasPotionEffect(PotionEffectType.SLOW_DIGGING))  {
							player.removePotionEffect(PotionEffectType.SLOW_DIGGING);
							if (player.hasMetadata("digging")) player.removeMetadata("digging", plugin);
						}
					} else if (!isStrong(player.getInventory().getItemInMainHand(), clicked)) {
						pie.setCancelled(true);
						player.addPotionEffect(new PotionEffect(PotionEffectType.SLOW_DIGGING, 100000, 255, false, false));
						player.setMetadata("digging", new FixedMetadataValue(plugin, "digging"));
					} else if (player.hasPotionEffect(PotionEffectType.SLOW_DIGGING)) {
						player.removePotionEffect(PotionEffectType.SLOW_DIGGING);
						if (player.hasMetadata("digging")) player.removeMetadata("digging", plugin);
					}
				} else if (player.hasPotionEffect(PotionEffectType.SLOW_DIGGING)) {
					player.removePotionEffect(PotionEffectType.SLOW_DIGGING);
					if (player.hasMetadata("digging")) player.removeMetadata("digging", plugin);
				}
			}
			
			if (pie.getAction() == Action.RIGHT_CLICK_AIR || pie.getAction() == Action.RIGHT_CLICK_BLOCK) {
				/*
				 * Waypoints
				 */
				if (player.getInventory().getItemInMainHand().getType() == Material.COMPASS) {
					player.setCompassTarget(player.getLocation());
					getConfig().set("Players.Waypoint." + player.getUniqueId() + ".world", player.getWorld());
					getConfig().set("Players.Waypoint." + player.getUniqueId() + ".x", player.getLocation().getX());
					getConfig().set("Players.Waypoint." + player.getUniqueId() + ".y", player.getLocation().getY());
					getConfig().set("Players.Waypoint." + player.getUniqueId() + ".z", player.getLocation().getZ());
					Message message = new Message(player, MessageType.valueOf(getConfig().getString("Server.Messages.Type")), Lang.WAYPOINT_SET, null);
					message.send();
				}
				
				/*
				 * Check for broken bones
				 */
				if (player.getInventory().getItemInMainHand().equals(bandage) && getConfig().getBoolean("Players.BoneBroke." + player.getUniqueId())) {
		            getConfig().set("Players.BoneBroke." + player.getUniqueId(), false);
	        		Message message = new Message(player, MessageType.valueOf(getConfig().getString("Server.Messages.Type")), Lang.USED_BANDAGE, null);
	        		message.send();
		            player.getInventory().removeItem(new ItemStack(player.getInventory().getItemInMainHand()));
		            player.updateInventory();
				} else if (pie.getAction().equals(Action.RIGHT_CLICK_BLOCK) && pie.getClickedBlock() != null && !pie.getClickedBlock().getType().equals(Material.AIR)) {
					if (beds.contains(pie.getClickedBlock().getType())) {
						/*
						 * Check for fatigue
						 */
						if (getConfig().getBoolean("Server.Player.Allow Fatigue") && getConfig().getInt("Players.Fatigue." + player.getUniqueId()) != 0) {
				            getConfig().set("Players.Fatigue." + player.getUniqueId(), 0);
			        		Message message = new Message(player, MessageType.valueOf(getConfig().getString("Server.Messages.Type")), Lang.NOT_TIRED, null);
			        		message.send();
						}
					}
				}
			}
		}
	}
	
	private boolean isStrong(ItemStack item, Block target) {
		if (target.getType() == Material.SNOW 
				|| target.getType() == Material.SNOW_BLOCK) {
			if (item.getType().toString().contains("SPADE")) return true;
		}
		
		List<Material> blacklist = Arrays.asList(Material.TORCH,
				Material.WALL_TORCH,
				Material.REDSTONE_TORCH,
				Material.REDSTONE_WALL_TORCH);
		
		if (blacklist.contains(target.getType())
				|| beds.contains(target.getType())) return true;
		
		IBlockData blockData = ((CraftWorld) target.getWorld()).getHandle().getType(new BlockPosition(target.getX(), target.getY(), target.getZ()));
		
		float speed = CraftItemStack.asNMSCopy(item).getItem().getDestroySpeed(CraftItemStack.asNMSCopy(item), blockData);
		return speed > 1.0;
	}
	
	private FileConfiguration getConfig() {
		return plugin.getConfig();
	}
}
