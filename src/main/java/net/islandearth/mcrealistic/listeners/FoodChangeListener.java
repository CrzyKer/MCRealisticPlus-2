/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 ******************************************************************************/
package net.islandearth.mcrealistic.listeners;

import java.util.List;

import org.bukkit.GameMode;
import org.bukkit.World;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.FoodLevelChangeEvent;

import net.islandearth.mcrealistic.MCRealistic;
import net.islandearth.mcrealistic.language.Lang;
import net.islandearth.mcrealistic.language.Message;
import net.islandearth.mcrealistic.language.MessageType;

public class FoodChangeListener implements Listener {
	
	private MCRealistic plugin;
	private List<World> worlds;
	
	public FoodChangeListener(MCRealistic plugin)
	{
		this.plugin = plugin;
		this.worlds = plugin.getWorlds();
	}
	
    @EventHandler
    public void onFoodChange(FoodLevelChangeEvent flce) {
        Player player = (Player) flce.getEntity();
		if(worlds.contains(player.getWorld())) 
		{
			if(player.getGameMode() != GameMode.CREATIVE && player.getGameMode() != GameMode.SPECTATOR) 
			{
				if (getConfig().getBoolean("Server.Player.DisplayHungerMessage") && player.getFoodLevel() < 6) 
				{
	        		Message m = new Message(player, MessageType.valueOf(getConfig().getString("Server.Messages.Type")), Lang.HUNGRY, null);
	        		m.send();
		    		int CurrentFatigue = getConfig().getInt("Players.Fatigue." + player.getUniqueId());
		    		getConfig().set("Players.Fatigue." + player.getUniqueId(), (++CurrentFatigue));
        		}
        	}
        }
    }
    
    private FileConfiguration getConfig()
    {
    	return plugin.getConfig();
    }
}
