/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 ******************************************************************************/
package net.islandearth.mcrealistic.listeners;

import java.util.Random;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

import net.islandearth.mcrealistic.MCRealistic;
import net.islandearth.mcrealistic.update.Updater;

public class JoinListener implements Listener {
	
	private MCRealistic plugin;
	
	public JoinListener(MCRealistic plugin) {
		this.plugin = plugin;
	}
	
	@EventHandler
	public void onJoin(PlayerJoinEvent pje) {
		Bukkit.getScheduler().runTask(plugin, () -> {
			Player player = pje.getPlayer();
			
			if (player.isOp()) {
				Bukkit.getScheduler().runTaskAsynchronously(plugin, () -> {
					Updater updater = new Updater(plugin);
					String updates = updater.getLatestVersion();
					if (!updates.equals(plugin.getDescription().getVersion())) {
					    player.sendMessage(ChatColor.GOLD + "[" + ChatColor.YELLOW + "MCRealistic" + ChatColor.GOLD + "] New update avaible:");
					    player.sendMessage(ChatColor.GOLD + "New version: " + ChatColor.YELLOW + updates);
					    player.sendMessage(ChatColor.GOLD + "Your version: " + ChatColor.YELLOW + plugin.getDescription().getVersion());
					}
				});
			}
			
			getConfig().set("Players.DefaultWalkSpeed." + player.getUniqueId(), 0.2);
			if (getConfig().getString("Players.Waypoint." + player.getUniqueId() + ".world") != null) {
				World world = Bukkit.getWorld(getConfig().getString("Players.Waypoint." + player.getUniqueId() + ".world"));
				int x = getConfig().getInt("Players.Waypoint." + player.getUniqueId() + ".x");
				int y = getConfig().getInt("Players.Waypoint." + player.getUniqueId() + ".y");
				int z = getConfig().getInt("Players.Waypoint." + player.getUniqueId() + ".z");
				
				Location location = new Location(world, x, y, z);
				player.setCompassTarget(location);
			}
			
			player.setWalkSpeed((float) 0.2);
			
	        if (!player.hasPlayedBefore()) {
	            getConfig().addDefault("Players.Thirst." + player.getUniqueId(), 0);
	            getConfig().addDefault("Players.RealName." + player.getUniqueId(), "null");
	            getConfig().addDefault("Players.IsCold." + player.getUniqueId(), false);
	            getConfig().addDefault("Players.InTorch." + player.getUniqueId(), false);
	            getConfig().addDefault("Players.Fatigue." + player.getUniqueId(), 0);
	            getConfig().set("Players.DefaultWalkSpeed." + player.getUniqueId(), 1);
	        }
	        
	        String[] FirstArray = plugin.getFirstNames().toArray(new String[20]);
	        Random random1 = new Random();
	        int randomFirstId = random1.nextInt(FirstArray.length);
	        String[] LastArray = plugin.getLastNames().toArray(new String[20]);
	        Random random2 = new Random();
	        int randomLastId = random2.nextInt(LastArray.length);
	        Random random3 = new Random();
	        int randomAge = random3.nextInt(50);
	        if (getConfig().getString("Players.RealName." + player.getUniqueId()) == null) getConfig().set("Players.RealName." + player.getUniqueId(), String.valueOf(String.valueOf(FirstArray[randomFirstId])) + " " + LastArray[randomLastId]);
	        if (getConfig().getString("Players.RealAge." + player.getUniqueId()) == null) getConfig().set("Players.RealAge." + player.getUniqueId(), randomAge + 15);
		});
	}
	
	private FileConfiguration getConfig() {
		return plugin.getConfig();
	}
}
