/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 ******************************************************************************/
package net.islandearth.mcrealistic.listeners;

import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.UUID;

import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerItemConsumeEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.PotionMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.potion.PotionType;

import net.islandearth.mcrealistic.MCRealistic;
import net.islandearth.mcrealistic.language.Lang;
import net.islandearth.mcrealistic.language.Message;
import net.islandearth.mcrealistic.language.MessageType;
import net.islandearth.mcrealistic.utils.TitleManager;

public class ConsumeListener implements Listener {
	
	private MCRealistic plugin;
	private List<World> worlds;
	
	public ConsumeListener(MCRealistic plugin) {
		this.plugin = plugin;
		this.worlds = plugin.getWorlds();
	}
	
	@EventHandler
	public void onItemConsume(PlayerItemConsumeEvent pice) {
		Player player = pice.getPlayer();
		ItemStack item = pice.getItem();
		if (worlds.contains(player.getWorld())) {
	        if (player.getGameMode() == GameMode.SURVIVAL) {
	        	if (item != null && item.getType() != null && player != null) {
	        		if (item.hasItemMeta() && item.getType().equals(Material.POTION)) {
	        			PotionMeta pm = (PotionMeta) item.getItemMeta();
	        			if (pm.getBasePotionData().getType() == PotionType.WATER) {
	        				if (getConfig().getBoolean("Server.Player.Thirst.Enabled")) {
		        				if (item.getItemMeta().hasLore()) {
				        			if (item.getItemMeta().getLore().get(0).equals(ChatColor.GRAY + "Purified")) {
				        				getConfig().set("Players.Thirst." + player.getUniqueId(), 0);
				    	        		Message m = new Message(player, MessageType.valueOf(getConfig().getString("Server.Messages.Type")), Lang.NOT_THIRSTY, null);
				    	        		m.send();
				    	        		
				    	        		int CurrentFatigue = this.getConfig().getInt("Players.Fatigue." + player.getUniqueId());
				    	        		getConfig().set("Players.Fatigue." + player.getUniqueId(), (CurrentFatigue -= 2));
				        			}
		        				} else {
		            				player.addPotionEffect(new PotionEffect(PotionEffectType.POISON, 120, 2));
		            				player.addPotionEffect(new PotionEffect(PotionEffectType.HUNGER, 180, 1));
		        				}
	        				}
	        			} else {
	        				if (item.getItemMeta().getDisplayName() != null) {	
	        					if (item.getItemMeta().getDisplayName().equals(ChatColor.GREEN + "Medicine") 
	        							&& item.getItemMeta().getLore().equals(Arrays.asList(ChatColor.WHITE + "Drink to help fight your cold/disease!"))) {
			        				
	        						List<UUID> hasDisease = getDiseases();
			        				List<UUID> hasCold = getColds();
			        				
			        				if (hasCold.contains(player.getUniqueId())) {
			        					TitleManager.sendTitle(player, "", ChatColor.GREEN + "The cold begins to subside...", 200);
			        					hasCold.remove(player.getUniqueId());
			        				} else if (hasDisease.contains(player.getUniqueId())) {
			        					hasDisease.remove(player.getUniqueId());
			        					TitleManager.sendTitle(player, "", ChatColor.GREEN + "The disease begins to subside...", 200);
			        				}
			        			}
		        			}
	        			}
		        	} else {
		        		if (item.getType() == Material.MILK_BUCKET 
		        				&& item.getItemMeta().getDisplayName().equals(ChatColor.DARK_GRAY + "" + ChatColor.BOLD + "Chocolate Milk") 
		        				&& item.getItemMeta().getLore().equals(Arrays.asList(ChatColor.WHITE + "Drink to gain Speed II."))) {
								player.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 2000, 1));
		            	} else if (isRaw(item.getType())) {
		    				Random random = new Random();
		    				int randomPoison = random.nextInt(2);
		    				
		    				switch (randomPoison) {
		    					case 0:
		    						break;
		    					case 1:
		    						player.addPotionEffect(new PotionEffect(PotionEffectType.POISON, 60, 1));
		    						player.addPotionEffect(new PotionEffect(PotionEffectType.HUNGER, 120, 0));
		    						break;
		    					case 2:
		    						break;
		    				}
		    			}	
		        	}
	        	}
	        }
		}
	}
	
	private boolean isRaw(Material material) {
		return material.toString().contains("RAW");
	}
	
	private List<UUID> getDiseases() {
		return plugin.getDiseases();
	}
	
	private List<UUID> getColds() {
		return plugin.getColds();
	}
	
	private FileConfiguration getConfig() {
		return plugin.getConfig();
	}
}
