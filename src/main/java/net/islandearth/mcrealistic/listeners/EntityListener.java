/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 ******************************************************************************/
package net.islandearth.mcrealistic.listeners;

import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.World;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;

import net.islandearth.mcrealistic.MCRealistic;
import net.islandearth.mcrealistic.language.Lang;
import net.islandearth.mcrealistic.language.Message;
import net.islandearth.mcrealistic.language.MessageType;

public class EntityListener implements Listener {
	
	private MCRealistic plugin;
	private List<World> worlds;
	
	public EntityListener(MCRealistic plugin)
	{
		this.plugin = plugin;
		this.worlds = plugin.getWorlds();
	}
	
    @EventHandler
    public void onPlayerFall(EntityDamageEvent ede) {
        if (ede.getEntity() instanceof Player) 
        {
            Player player = (Player)ede.getEntity();
    		if(worlds.contains(player.getWorld()) && plugin.getConfig().getBoolean("Server.Player.Broken_Bones.Enabled")) 
    		{
    			if(player.getGameMode() != GameMode.CREATIVE && player.getGameMode() != GameMode.SPECTATOR) 
    			{
    				if (ede.getCause() == EntityDamageEvent.DamageCause.FALL 
    						&& player.getFallDistance() >= 7.0f
    						&& !ede.isCancelled()) {
    					getConfig().set("Players.BoneBroke." + player.getUniqueId(), true);
    	        		Message m = new Message(player, MessageType.valueOf(getConfig().getString("Server.Messages.Type")), Lang.BROKEN_BONES, null);
    	        		m.send();
    					getConfig().set("Players.DefaultWalkSpeed." + player.getPlayer().getUniqueId(), 0.13);
    					float WeightLeggings = getConfig().getInt("Players.LeggingsWeight." + player.getUniqueId());
    					float WeightChestPlate = getConfig().getInt("Players.ChestplateWeight." + player.getUniqueId());
    					float WeightCombined = WeightLeggings + WeightChestPlate;
    					player.setWalkSpeed((float)(getConfig().getDouble("Players.DefaultWalkSpeed." + player.getUniqueId()) - (double)(WeightCombined * 0.01f)));
    					Float floatObj = Float.valueOf(player.getFallDistance() * 80.0f);
    					Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable()
    					{
    						@Override
    						public void run() 
    						{
    							if (getConfig().getBoolean("Players.BoneBroke." + player.getUniqueId())) 
    							{
    				        		Message m = new Message(player, MessageType.valueOf(getConfig().getString("Server.Messages.Type")), Lang.LEGS_HEALED, null);
    				        		m.send();
    								getConfig().set("Players.DefaultWalkSpeed." + player.getPlayer().getUniqueId(), 0.2);
    								float WeightLeggings = getConfig().getInt("Players.LeggingsWeight." + player.getUniqueId());
    								float WeightChestPlate = getConfig().getInt("Players.ChestplateWeight." + player.getUniqueId());
    								float WeightCombined = WeightLeggings + WeightChestPlate;
    								player.setWalkSpeed((float)(getConfig().getDouble("Players.DefaultWalkSpeed." + player.getPlayer().getUniqueId()) - (double)(WeightCombined * 0.01f)));
    								getConfig().set("Players.BoneBroke." + player.getUniqueId(), false);
    							}
    						}
    					}, floatObj.longValue());
    				}
    			}
    		}
        }
    }
    
    private FileConfiguration getConfig()
    {
    	return plugin.getConfig();
    }
}
