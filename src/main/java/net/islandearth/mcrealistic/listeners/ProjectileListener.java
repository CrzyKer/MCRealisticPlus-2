/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 ******************************************************************************/
package net.islandearth.mcrealistic.listeners;

import java.util.List;

import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Projectile;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.ProjectileHitEvent;

import com.sk89q.worldedit.bukkit.BukkitAdapter;
import com.sk89q.worldguard.WorldGuard;
import com.sk89q.worldguard.protection.ApplicableRegionSet;
import com.sk89q.worldguard.protection.regions.RegionQuery;

import net.islandearth.mcrealistic.MCRealistic;


public class ProjectileListener implements Listener {
	
	private MCRealistic plugin;
	private WorldGuard wg;
	private List<World> worlds;
	
	public ProjectileListener(MCRealistic plugin) {
		this.plugin = plugin;
		this.wg = plugin.getWorldGuard();
		this.worlds = plugin.getWorlds();
	}
	
    @EventHandler
    public void onProjectileHit(ProjectileHitEvent phe) {
    	Projectile p = phe.getEntity();
        RegionQuery q = wg.getPlatform().getRegionContainer().createQuery();
        ApplicableRegionSet ars = q.getApplicableRegions(BukkitAdapter.adapt(p.getLocation()));
		if (worlds.contains(p.getWorld()) && ars.getRegions().size() == 0) {
			if (getConfig().getBoolean("Server.Player.Allow Enchanted Arrow") 
					&& p.getType() == EntityType.ARROW 
					&& p.getFireTicks() > 0) {
				if (p.getWorld().getBlockAt(p.getLocation()).getType() == null 
						|| p.getWorld().getBlockAt(p.getLocation()).getType().equals(Material.AIR)) {
					p.getWorld().getBlockAt(p.getLocation()).setType(Material.FIRE);
				}
			}
    	}
    }
    
    private FileConfiguration getConfig() {
    	return plugin.getConfig();
    }
}
