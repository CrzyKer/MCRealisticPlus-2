/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 ******************************************************************************/
package net.islandearth.mcrealistic.listeners;

import java.util.List;
import java.util.Random;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.inventory.ItemStack;

import net.islandearth.mcrealistic.MCRealistic;


public class RespawnListener implements Listener {
	
	private MCRealistic plugin;
	private List<World> worlds;
	
	public RespawnListener(MCRealistic plugin)
	{
		this.plugin = plugin;
		this.worlds = plugin.getWorlds();
	}
	
	@EventHandler
    public void onPlayerSpawn(PlayerRespawnEvent pre) 
	{
        Player p = pre.getPlayer();
		if(worlds.contains(p.getWorld())) 
		{
	        getConfig().set("Players.Thirst." + p.getUniqueId(), 0);
	        if(getConfig().getBoolean("Server.Player.Spawn with items")) 
	        {
	            p.getInventory().addItem(new ItemStack(Material.GLASS_BOTTLE));
	            p.getInventory().addItem(new ItemStack(Material.WOODEN_AXE));
	        }
	        
	        /*
	         * Get names
	         */
	        String[] FirstArray = getFirstNames().toArray(new String[20]);
	        Random random1 = new Random();
	        int randomFirstId = random1.nextInt(FirstArray.length);
	        String[] LastArray = getLastNames().toArray(new String[20]);
	        Random random2 = new Random();
	        int randomLastId = random2.nextInt(LastArray.length);
	        Random random3 = new Random();
	        int randomAge = random3.nextInt(50);
	        
	        if(getConfig().getBoolean("Server.Messages.Respawn")) 
	        {
	        	p.sendMessage(ChatColor.GOLD + "Type /mystats to see your stats!");
	        	p.sendMessage(ChatColor.GOLD + "Type /fatigue to see your fatigue!");
	        }
	        
	        getConfig().set("Players.RealName." + p.getUniqueId(), String.valueOf(String.valueOf(FirstArray[randomFirstId])) + " " + LastArray[randomLastId]);
	        getConfig().set("Players.RealAge." + p.getUniqueId(), randomAge + 15);
		}
    }
	
	private List<String> getFirstNames()
	{
		return plugin.getFirstNames();
	}
	
	private List<String> getLastNames()
	{
		return plugin.getLastNames();
	}
	
	private FileConfiguration getConfig()
	{
		return plugin.getConfig();
	}
}
