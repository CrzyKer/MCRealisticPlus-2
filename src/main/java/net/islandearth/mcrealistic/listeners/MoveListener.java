/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 ******************************************************************************/
package net.islandearth.mcrealistic.listeners;

import java.util.List;
import java.util.Random;
import java.util.UUID;

import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;

import com.sk89q.worldedit.bukkit.BukkitAdapter;
import com.sk89q.worldguard.WorldGuard;
import com.sk89q.worldguard.protection.ApplicableRegionSet;
import com.sk89q.worldguard.protection.regions.RegionQuery;

import net.islandearth.mcrealistic.MCRealistic;

public class MoveListener implements Listener {
	
	private MCRealistic plugin;
	private WorldGuard wg;
	private List<World> worlds;
	private List<UUID> burn;
	
	public MoveListener(MCRealistic plugin) {
		this.plugin = plugin;
		this.wg = plugin.getWorldGuard();
		this.worlds = plugin.getWorlds();
		this.burn = plugin.getBurning();
	}
	
	@EventHandler
	public void PlayerMove(PlayerMoveEvent pme) {
		Player player = pme.getPlayer();
		if (worlds.contains(player.getWorld())) {
			Random rand = new Random();
			int max = 20;
			int min = 1;
			int randomNum = rand.nextInt((max - min) + 1) + min;
			Location blockunder = player.getLocation();
			blockunder.subtract(0, 0.9, 0);
			Block b = blockunder.getBlock();
			Location blockabove = player.getLocation();
			blockabove.setY(blockabove.getY() + 10);
			if (player.getGameMode() == GameMode.SURVIVAL) {
		        RegionQuery q = wg.getPlatform().getRegionContainer().createQuery();
		        ApplicableRegionSet ars = q.getApplicableRegions(BukkitAdapter.adapt(player.getLocation()));
		        if (player.isOnGround() 
		        		&& getConfig().getBoolean("Server.Player.Trail.Enabled") 
		        		&& ars.getRegions().size() == 0
		        		&& pme.getTo().distance(pme.getFrom()) > 0.0) {
					//no longer exists player.getWorld().playEffect(trail, Effect.FOOTSTEP 1);
					if (randomNum == 3 && b.getType() == Material.GRASS_BLOCK && getConfig().getBoolean("Server.Player.Path")) {
						List<String> g = getConfig().getStringList("Server.Player.Trail.Grass_Blocks");
						Material m = Material.valueOf(g.get(new Random().nextInt(g.size())));
						b.setType(m);
					} else if (randomNum == 1 && b.getType() == Material.SAND && getConfig().getBoolean("Server.Player.Path")) {
						List<String> s = getConfig().getStringList("Server.Player.Trail.Sand_Blocks");
						Material m = Material.valueOf(s.get(new Random().nextInt(s.size())));
						b.setType(m);
					} else if (randomNum == 7 && (b.getType() == Material.DIRT || b.getType() == Material.COARSE_DIRT) && getConfig().getBoolean("Server.Player.Path")) {
						List<String> d = getConfig().getStringList("Server.Player.Trail.Dirt_Blocks");
						Material m = Material.valueOf(d.get(new Random().nextInt(d.size())));
						b.setType(m);
					} else if (randomNum == 13 && b.getType() == Material.GRASS_PATH && getConfig().getBoolean("Server.Player.Path")) {
						List<String> d = getConfig().getStringList("Server.Player.Trail.Path_Blocks");
						Material m = Material.valueOf(d.get(new Random().nextInt(d.size())));
						b.setType(m);
					}
				}
				
				if ((double)player.getWalkSpeed() > getConfig().getDouble("Players.DefaultWalkSpeed." + player.getUniqueId())) {
					player.setWalkSpeed((float)getConfig().getDouble("Players.DefaultWalkSpeed." + player.getUniqueId()));
				}
				
				if (player.getLocation().getBlock().getType() == Material.TORCH) {
					if (!burn.contains(player.getUniqueId())) {
						burn.add(player.getUniqueId());
					}
				} else if (burn.contains(player.getUniqueId())) {
					player.setFireTicks(0);
					burn.remove(player.getUniqueId());
				}
			}
		}
	}
	
	private FileConfiguration getConfig() {
		return plugin.getConfig();
	}
}
