/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 ******************************************************************************/
package net.islandearth.mcrealistic.listeners;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;

import net.islandearth.mcrealistic.MCRealistic;
import net.islandearth.mcrealistic.ui.UI;
import net.islandearth.mcrealistic.ui.UI.ItemClick;

public class InventoryListener implements Listener {
	
	private MCRealistic plugin;
	private static int[] $SWITCH_TABLE$org$bukkit$Material;
	
	public InventoryListener(MCRealistic plugin)
	{
		this.plugin = plugin;
	}
	
    @EventHandler
    public void onClose(InventoryCloseEvent ice) {
        if(ice.getPlayer() instanceof Player) 
        {
        	if(getConfig().getBoolean("Server.Player.Weight"))
        	{
	            Player pl = (Player)ice.getPlayer();
	            List<String> worlds = new ArrayList<>();
	            worlds = getConfig().getStringList("Worlds");
	    		if(worlds.contains(pl.getWorld().getName()))
	    		{
	    			UI ui = UI.getInventories().get(UI.getOpen().get(pl.getUniqueId()));
	    			
	    			if(ui != null) ui.delete();
	    			if(UI.getOpen().containsKey(pl.getUniqueId())) UI.getOpen().remove(pl.getUniqueId());
	    			
	    			if(pl.getGameMode() != GameMode.CREATIVE && pl.getGameMode() != GameMode.SPECTATOR) 
	    			{
	    				pl.setWalkSpeed(0.2f);
	    				if(pl.getInventory().getChestplate() != null) 
	    				{
	    					switch ($SWITCH_TABLE$org$bukkit$Material()[pl.getInventory().getChestplate().getType().ordinal()]) 
	    					{
		    					case 250: {
		    						getConfig().set("Players.ChestplateWeight." + pl.getUniqueId(), 5);
		    						break;
		    					}
		    					case 246: {
		    						getConfig().set("Players.ChestplateWeight." + pl.getUniqueId(), 5);
		    						break;
		    					}
		    					case 258: {
		    						getConfig().set("Players.ChestplateWeight." + pl.getUniqueId(), 6);
		    						break;
		    					}
		    					case 254: {
		    						getConfig().set("Players.ChestplateWeight." + pl.getUniqueId(), 8);
		    						break;
		    					}
		    					default: {
		    						getConfig().set("Players.ChestplateWeight." + pl.getUniqueId(), 0);
		    						break;
		    					}
	    					}
	    				} else getConfig().set("Players.ChestplateWeight." + pl.getUniqueId(), 0);
	    				if (pl.getInventory().getLeggings() != null) 
	    				{
							switch ($SWITCH_TABLE$org$bukkit$Material()[pl.getInventory().getLeggings().getType().ordinal()]) 
							{
								case 251: 
								{
									getConfig().set("Players.LeggingsWeight" + pl.getUniqueId(), 5);
									break;
								}
								case 247: 
								{
									getConfig().set("Players.LeggingsWeight" + pl.getUniqueId(), 5);
									break;
								}
								case 259: 
								{
									getConfig().set("Players.LeggingsWeight" + pl.getUniqueId(), 6);
									break;
								}
								case 255: 
								{
									getConfig().set("Players.LeggingsWeight" + pl.getUniqueId(), 8);
									break;
								}
								default: 
								{
									getConfig().set("Players.LeggingsWeight." + pl.getUniqueId(), 0);
									break;
								}
							}
						} else getConfig().set("Players.LeggingsWeight." + pl.getUniqueId(), 0);
	    				float WeightLeggings = getConfig().getInt("Players.LeggingsWeight." + pl.getUniqueId());
	    				float WeightChestPlate = getConfig().getInt("Players.ChestplateWeight." + pl.getUniqueId());
	    				float WeightCombined = WeightLeggings + WeightChestPlate;
	    				pl.setWalkSpeed((float)(getConfig().getDouble("Players.DefaultWalkSpeed." + ice.getPlayer().getUniqueId()) - (double)(WeightCombined * 0.01f)));
	    			}	
	    		}	
        	}
        }
    }
    
    @EventHandler
	public void onClick(InventoryClickEvent ice)
	{
		if(ice.getWhoClicked() instanceof Player)
		{
			Player player = (Player) ice.getWhoClicked();
			UUID uuid = UI.getOpen().get(player.getUniqueId());
			if(uuid != null)
			{
				UI ui = UI.getInventories().get(uuid);
				ice.setCancelled(true);
				ItemClick action = ui.getActions().get(ice.getSlot());
				
				if(action != null)
				{
					action.click(player);
				}
			}
		}
	}
    
    private static int[] $SWITCH_TABLE$org$bukkit$Material() {
        int[] arrn2 = $SWITCH_TABLE$org$bukkit$Material;
        if (arrn2 != null) {
            return arrn2;
        }
        
        int[] arrn = new int[Material.values().length];
            arrn[Material.CHAINMAIL_BOOTS.ordinal()] = 248;
            arrn[Material.CHAINMAIL_CHESTPLATE.ordinal()] = 246;
            arrn[Material.CHAINMAIL_HELMET.ordinal()] = 245;
            arrn[Material.CHAINMAIL_LEGGINGS.ordinal()] = 247;
            arrn[Material.DIAMOND_BOOTS.ordinal()] = 256;
            arrn[Material.DIAMOND_CHESTPLATE.ordinal()] = 254;
            arrn[Material.DIAMOND_HELMET.ordinal()] = 253;
            arrn[Material.DIAMOND_LEGGINGS.ordinal()] = 255;
            arrn[Material.GOLDEN_BOOTS.ordinal()] = 260;
            arrn[Material.GOLDEN_CHESTPLATE.ordinal()] = 258;
            arrn[Material.GOLDEN_HELMET.ordinal()] = 257;
            arrn[Material.GOLDEN_LEGGINGS.ordinal()] = 259;
            arrn[Material.IRON_BOOTS.ordinal()] = 252;
            arrn[Material.IRON_CHESTPLATE.ordinal()] = 250;
            arrn[Material.IRON_HELMET.ordinal()] = 249;
            arrn[Material.IRON_LEGGINGS.ordinal()] = 251;
            arrn[Material.LEATHER_BOOTS.ordinal()] = 244;
            arrn[Material.LEATHER_CHESTPLATE.ordinal()] = 242;
            arrn[Material.LEATHER_HELMET.ordinal()] = 241;
            arrn[Material.LEATHER_LEGGINGS.ordinal()] = 243;
        
        $SWITCH_TABLE$org$bukkit$Material = arrn;
        return $SWITCH_TABLE$org$bukkit$Material;
    }
    
    private FileConfiguration getConfig()
    {
    	return plugin.getConfig();
    }
}
