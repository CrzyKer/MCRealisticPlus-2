/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 ******************************************************************************/
package net.islandearth.mcrealistic.commands;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.defaults.BukkitCommand;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

import net.islandearth.mcrealistic.MCRealistic;
import net.islandearth.mcrealistic.language.Lang;
import net.islandearth.mcrealistic.utils.Utils;

public class MyStats extends BukkitCommand {
	
	private MCRealistic plugin;
	private String c = "[MCRealistic-2] ";
	
	public MyStats(MCRealistic plugin) {
		super("MyStats");
		this.description = "MCRealistic stats";
		this.usageMessage = "/MyStats";
		this.plugin = plugin;
		this.c = "[MCRealistic-2] ";
	}

	@Override
	public boolean execute(CommandSender sender, String arg1, String[] args) {
		if (sender instanceof Player) {
			Player player = (Player) sender;
			if (Utils.isWorldEnabled(player.getWorld())) {
				if (!player.hasPermission("mcr.mystats")) {
					player.sendMessage(Lang.NO_PERMISSION.getValue(player, new String[] {"mcr.mystats"}));
				} else if (getConfig().getBoolean("Server.Player.Allow /fatigue")) {
					player.sendMessage(ChatColor.GOLD + "==== " + ChatColor.DARK_GREEN + "My stats are: " + ChatColor.GOLD + " ====");
					player.sendMessage(ChatColor.AQUA + "My name is: " + getConfig().getString(new StringBuilder("Players.RealName.").append(player.getUniqueId()).toString()));
					player.sendMessage(ChatColor.AQUA + "My nickname is: " + player.getDisplayName());
					player.sendMessage(ChatColor.AQUA + "My age is: " + getConfig().getInt(new StringBuilder("Players.RealAge.").append(player.getUniqueId()).toString()));
					player.sendMessage(ChatColor.GOLD + "====================");
					return true;
				}
			} else {
				player.sendMessage(ChatColor.RED + "MyStats is not enabled in this world.");
			}
		} else {
			sender.sendMessage(c + "You must be a player to execute this command.");
		}
		
		return true;
	}
	
	private FileConfiguration getConfig() {
		return plugin.getConfig();
	}
}
