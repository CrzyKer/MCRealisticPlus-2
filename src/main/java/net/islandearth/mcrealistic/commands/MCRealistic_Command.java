/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 ******************************************************************************/
package net.islandearth.mcrealistic.commands;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.CommandSender;
import org.bukkit.command.defaults.BukkitCommand;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import net.islandearth.mcrealistic.MCRealistic;
import net.islandearth.mcrealistic.language.Lang;
import net.islandearth.mcrealistic.ui.ItemUI;
import net.islandearth.mcrealistic.utils.Utils;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;

public class MCRealistic_Command extends BukkitCommand {

	private MCRealistic plugin;
	private String c = "[MCRealistic-2] ";
	
	public MCRealistic_Command(MCRealistic plugin) {
		super("MCRealistic");
		this.description = "MCRealistic commands";
		this.usageMessage = "/MCRealistic";
		this.setAliases(Arrays.asList("mcr", "mcreal", "mcrc"));
		this.plugin = plugin;
		this.c = "[MCRealistic-2] ";
	}

	@Override
	public boolean execute(CommandSender sender, String commandLabel, String[] args) {
		if (sender instanceof Player) {
			Player player = (Player) sender;
			if (Utils.isWorldEnabled(player.getWorld())) {
				if (args.length == 0) {
					TextComponent help = new TextComponent(ChatColor.YELLOW + "Showing help for MCRealistic " + ChatColor.WHITE + "1/1");
					help.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder(ChatColor.WHITE + "Showing page 1/1 click to go to the next page or use /mcr 2.").create()));
					//help.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/mcr 2"));
					player.spigot().sendMessage(help);
					TextComponent gitlab = new TextComponent(ChatColor.YELLOW + "GitLab: " + ChatColor.WHITE + "https://gitlab.com/SamB440/MCRealisticPlus-2");
					gitlab.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder(ChatColor.WHITE + "Click to view our gitlab!").create()));
					gitlab.setClickEvent(new ClickEvent(ClickEvent.Action.OPEN_URL, "https://gitlab.com/SamB440/MCRealisticPlus-2"));
					player.spigot().sendMessage(gitlab);
					player.spigot().sendMessage(help);
					TextComponent c1 = new TextComponent(ChatColor.GREEN + "/MCRealistic");
					c1.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder(ChatColor.WHITE + "Click to paste command.").create()));
					c1.setClickEvent(new ClickEvent(ClickEvent.Action.SUGGEST_COMMAND, "/MCRealistic"));
					player.spigot().sendMessage(c1);
					player.sendMessage(ChatColor.WHITE + "   Aliases: /mcr, mcrc, mcreal.");
					player.sendMessage(ChatColor.WHITE + "   Description: Displays help page.");
					TextComponent c2 = new TextComponent(ChatColor.GREEN + "/MCRealistic reload");
					c2.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder(ChatColor.WHITE + "Click to paste command.").create()));
					c2.setClickEvent(new ClickEvent(ClickEvent.Action.SUGGEST_COMMAND, "/MCRealistic reload"));
					player.spigot().sendMessage(c2);
					player.sendMessage(ChatColor.WHITE + "   Aliases: None.");
					player.sendMessage(ChatColor.WHITE + "   Description: Reload the plugin.");
					player.sendMessage(ChatColor.WHITE + "   Permission(s): OP");
					TextComponent c3 = new TextComponent(ChatColor.GREEN + "/MCRealistic item <item> <amount>");
					c3.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder(ChatColor.WHITE + "Click to paste command.").create()));
					c3.setClickEvent(new ClickEvent(ClickEvent.Action.SUGGEST_COMMAND, "/MCRealistic item <item> 1"));
					player.spigot().sendMessage(c3);
					player.sendMessage(ChatColor.WHITE + "   Aliases: None.");
					player.sendMessage(ChatColor.WHITE + "   Description: Get an item with an amount.");
					player.sendMessage(ChatColor.WHITE + "   Permission(s): mcr.give.item");
					player.sendMessage(ChatColor.YELLOW + "© 2018 IslandEarth. All rights reserved. Made with" + " ❤ " + "by SamB440.");
				} else if(args.length == 1) {
					if(args[0].equalsIgnoreCase("reload"))
					{
						if (!player.hasPermission("mcr.reload")) {
							player.sendMessage(Lang.NO_PERMISSION.getValue(player, new String[] {"mcr.reload"}));
						} else {
							player.sendMessage(ChatColor.GREEN + "Reloading...");
							plugin.reloadConfig();
							plugin.saveConfig();
							
							for (Player pl : Bukkit.getOnlinePlayers()) {
								plugin.getConfig().set("Players.DefaultWalkSpeed." + pl.getUniqueId(), 0.2);
								pl.setWalkSpeed((float) plugin.getConfig().getDouble("Players.DefaultWalkSpeed." + pl.getUniqueId()));
							}
							
							player.sendMessage(ChatColor.GREEN + "Done!");
						}
					} else if(args[0].equalsIgnoreCase("items")) {
						if (!player.hasPermission("mcr.items")) {
							player.sendMessage(Lang.NO_PERMISSION.getValue(player, new String[] {"mcr.items"}));
						} else {
							
							ItemStack medicine = new ItemStack(Material.POTION, 2);
							ItemMeta medicinemeta = medicine.getItemMeta();
							medicinemeta.setDisplayName(ChatColor.GREEN + "Medicine");
							medicinemeta.setLore(Arrays.asList(ChatColor.WHITE + "Drink to help fight your cold/disease!"));
							medicine.setItemMeta(medicinemeta);
							
							ItemStack chocolatemilk = new ItemStack(Material.MILK_BUCKET);
							ItemMeta chocolatemilkmeta = chocolatemilk.getItemMeta();
							chocolatemilkmeta.setDisplayName(ChatColor.DARK_GRAY + "" + ChatColor.BOLD + "Chocolate Milk");
							chocolatemilkmeta.setLore(Arrays.asList(ChatColor.WHITE + "Drink to gain Speed II."));
							chocolatemilk.setItemMeta(chocolatemilkmeta);
							
							ItemStack bandage = new ItemStack(Material.PAPER);
							ItemMeta bm = bandage.getItemMeta();
							bm.setDisplayName(ChatColor.DARK_AQUA + "Bandage");
						    bandage.setItemMeta(bm);
						    
						    ItemStack chocolate = plugin.getSkull("http://textures.minecraft.net/texture/a829b238f2ca2f39f2ce43e316e88e8ddfed524a5dfffefafbe8161dd9f93e9");
						    ItemMeta cm = chocolate.getItemMeta();
						    cm.setDisplayName("Chocolate");
						    chocolate.setItemMeta(cm);
						    
						    ItemStack milkchocolate = plugin.getSkull("http://textures.minecraft.net/texture/a829b238f2ca2f39f2ce43e316e88e8ddfed524a5dfffefafbe8161dd9f93e9");
						    ItemMeta mcm = milkchocolate.getItemMeta();
						    mcm.setDisplayName("Milk Chocolate");
						    milkchocolate.setItemMeta(mcm);
						    
						    ItemStack stick = new ItemStack(Material.STICK);
						    ItemMeta sm = stick.getItemMeta();
						    sm.setDisplayName(ChatColor.WHITE + "Digging Stick");
						    sm.setLore(Arrays.asList(ChatColor.WHITE + "Use this to break gravel and make an axe."));
						    stick.setItemMeta(sm);
						    
							List<ItemStack> items = Arrays.asList(medicine, chocolatemilk, bandage, chocolate, milkchocolate, stick);
							
							new ItemUI(items).openInventory(player);
							return true;
						}
					} else if (args[0].equalsIgnoreCase("update")) {
						player.sendMessage(ChatColor.RED + "Sorry, this feature is no longer available!");
					}
				} else if(args.length == 3) {
					if (args[0].equalsIgnoreCase("item")) {
						if (player.hasPermission("mcr.item.give")) {
							
							switch (args[1].toLowerCase()) {
								case "bandage": {
									ItemStack Bandage = new ItemStack(Material.PAPER, Integer.parseInt(args[2]));
									ItemMeta BandageItemMeta = Bandage.getItemMeta();
									BandageItemMeta.setDisplayName(ChatColor.DARK_AQUA + "Bandage");
									Bandage.setItemMeta(BandageItemMeta);
									player.getInventory().addItem(Bandage);
									break;
								}
								
								case "chocolatemilk": {
									ItemStack chocolatemilk = new ItemStack(Material.MILK_BUCKET, Integer.parseInt(args[2]));
									ItemMeta chocolatemilkmeta = chocolatemilk.getItemMeta();
									chocolatemilkmeta.setDisplayName(ChatColor.DARK_GRAY + "" + ChatColor.BOLD + "Chocolate Milk");
									chocolatemilkmeta.setLore(Arrays.asList(ChatColor.WHITE + "Drink to gain Speed II."));
									chocolatemilk.setItemMeta(chocolatemilkmeta);
									player.getInventory().addItem(chocolatemilk);
									break;
								}
								
								case "medicine": {
									ItemStack medicine = new ItemStack(Material.POTION, Integer.parseInt(args[2]));
									ItemMeta medicinemeta = medicine.getItemMeta();
									medicinemeta.setDisplayName(ChatColor.GREEN + "Medicine");
									medicinemeta.setLore(Arrays.asList(ChatColor.WHITE + "Drink to help fight your cold/disease!"));
									medicine.setItemMeta(medicinemeta);
									player.getInventory().addItem(medicine);
									break;
								}
							}
						} else {
							player.sendMessage(Lang.NO_PERMISSION.getValue(player, new String[] {"mcr.item.give"}));
						}
					}
				}
			} else {
				player.sendMessage(ChatColor.RED + "MCRealistic is not enabled in this world.");
			}
		} else {
			sender.sendMessage(c + "You must be a player to execute this command.");
		}
		
		return true;
	}
	
	@Override
	public List<String> tabComplete(CommandSender sender, String alias, String[] args) {
		if (!(sender instanceof Player)) return Arrays.asList(ChatColor.RED + "You must be a player to execute this command.");
		
		Player player = (Player) sender;
		if (args.length == 1) {
			List<String> possible = Arrays.asList("reload", "items", "item");
			if (!args[0].equals("")) {
				switch (args[0].toLowerCase()) {
					case "items":
						if (sender.hasPermission("mcr.items")) {
							return null;
						} else {
							return Arrays.asList(Lang.NO_PERMISSION.getValue(player, new String[] {"mcr.items"}));
						}
					case "reload":
						if (sender.hasPermission("mcr.reload")) {
							return null;
						} else {
							return Arrays.asList(Lang.NO_PERMISSION.getValue(player, new String[] {"mcr.reload"}));
						}
						
					default:
						
						List<String> results = new ArrayList<String>();
						
						for (String string : possible) {
							Pattern pattern = Pattern.compile(args[0]);
							Matcher matcher = pattern.matcher(string);
							
							while (matcher.find()) {
								results.add(string);
							}
						}
						
						if (!results.isEmpty()) return results;
						return Arrays.asList(ChatColor.RED + "Unknown command.");
				}
			} else return possible;
		} else if (args.length == 2) {
			if (args[0].equalsIgnoreCase("item")) {
				List<String> possible = Arrays.asList("bandage", "chocolatemilk", "medicine");
				if (sender.hasPermission("mcr.item.give")) {
					List<String> results = new ArrayList<String>();
					
					for (String string : possible) {
						Pattern pattern = Pattern.compile(args[1]);
						Matcher matcher = pattern.matcher(string);
						
						while (matcher.find()) {
							results.add(string);
						}
					}
					
					if (!results.isEmpty()) return results;
					return Arrays.asList(ChatColor.RED + "Unknown command.");
				} else {
					return Arrays.asList(Lang.NO_PERMISSION.getValue(player, new String[] {"mcr.item.give"}));
				}
			} else {
				return Arrays.asList(ChatColor.RED + "Unknown command.");
			}
		} else if(args.length == 3) {
			if (args[0].equalsIgnoreCase("item")) {
				return Arrays.asList("1", "10", "64", "...");
			}
		} return null;
	}
}
