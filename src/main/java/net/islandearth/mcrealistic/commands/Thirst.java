/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 ******************************************************************************/
package net.islandearth.mcrealistic.commands;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.defaults.BukkitCommand;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

import net.islandearth.mcrealistic.MCRealistic;
import net.islandearth.mcrealistic.language.Lang;
import net.islandearth.mcrealistic.utils.Utils;

public class Thirst extends BukkitCommand {
	
	private MCRealistic plugin;
	private String c = "[MCRealistic-2] ";
	
	public Thirst(MCRealistic plugin) {
		super("Thirst");
		this.description = "MCRealistic thirst";
		this.usageMessage = "/Thirst";
		this.plugin = plugin;
		this.c = "[MCRealistic-2] ";
	}

	@Override
	public boolean execute(CommandSender sender, String arg1, String[] args) {
		if (sender instanceof Player) {
			Player player = (Player) sender;
			if (Utils.isWorldEnabled(player.getWorld())) {
				if (!player.hasPermission("mcr.thirst")) {
					player.sendMessage(Lang.NO_PERMISSION.getValue(player, new String[] {"mcr.thirst"}));
				} else if(getConfig().getBoolean("Server.Player.Allow /thirst")) {
					player.sendMessage(ChatColor.GOLD + "==== " + ChatColor.DARK_GREEN + "My thirst is: " + ChatColor.GREEN + getConfig().getInt(new StringBuilder("Players.Thirst.").append(player.getUniqueId()).toString()) + "/200" + ChatColor.GOLD + " ====");
					return true;
				}
			} else {
				player.sendMessage(ChatColor.RED + "Thirst is not enabled in this world.");
			}
		} else {
			sender.sendMessage(c + "You must be a player to execute this command.");
		}
		
		return true;
	}
	
	private FileConfiguration getConfig() {
		return plugin.getConfig();
	}
}
